package dev.stok.indicator

import dev.stok.typing.Candle
import dev.stok.typing.InstrumentId

interface InstrumentCandleSource : IndicatorValueSource<Candle> {

    val instrumentId: InstrumentId
}
