package dev.stok.indicator.impl

import dev.stok.indicator.ComputableArray
import dev.stok.indicator.IndicatorValue
import dev.stok.indicator.IndicatorValueSource
import dev.stok.indicator.util.computeCandleMoment
import dev.stok.typing.CandleInterval
import kotlinx.datetime.Instant
import kotlin.time.Duration

open class ValueSourceArray<T : Any>(
    private val valueSource: IndicatorValueSource<T>,
    override val interval: CandleInterval,
    override val dateRange: Duration
) : ComputableArray<T> {

    override suspend fun getCustomDateRange(fromMoment: Instant, toMoment: Instant): List<IndicatorValue<T>> {
        require(fromMoment <= toMoment) { "Moment 'to' must follow after 'before' moment" }
        val effectiveFrom = computeCandleMoment(fromMoment, interval)
        val effectiveTo = computeCandleMoment(toMoment, interval)
        return valueSource.getValues(interval, effectiveFrom, effectiveTo)
    }
}
