package dev.stok.indicator

import dev.stok.common.definition.InternalLibraryApi
import dev.stok.typing.CandleInterval
import kotlinx.datetime.Instant
import kotlin.time.Duration

interface ComputableArray<T : Any> {

    val interval: CandleInterval

    val dateRange: Duration

    suspend fun getDateRange(toMoment: Instant): List<IndicatorValue<T>> =
        getCustomDateRange(
            fromMoment = toMoment - dateRange,
            toMoment = toMoment,
        )

    @InternalLibraryApi
    suspend fun getCustomDateRange(fromMoment: Instant, toMoment: Instant): List<IndicatorValue<T>>
}
