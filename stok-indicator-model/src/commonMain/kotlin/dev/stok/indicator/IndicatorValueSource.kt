package dev.stok.indicator

import dev.stok.typing.CandleInterval
import kotlinx.datetime.Instant

fun interface IndicatorValueSource<out T : Any> {

    /**
     * Gets list of indicator values from the given source.
     *
     * The first value will have [IndicatorValue.moment] == `from`
     * The last value will have [IndicatorValue.moment] == `to`
     *
     * List size MUST be `((to - from) / interval) + 1`
     * List elements MUST be sorted by [IndicatorValue.moment] in ascending order.
     * First list element MUST have value [from] for [IndicatorValue.moment]
     * Last list element MUST have value [to] for [IndicatorValue.moment]
     */
    suspend fun getValues(interval: CandleInterval, from: Instant, to: Instant): List<IndicatorValue<T>>
}
