plugins {
    id(BuildPlugins.STOK_LIBRARY)
}

kotlin {
    sourceSets {
        commonMain {
            dependencies {
                api(projects.stokDomainModel)
                api(projects.stokIntegrationModel)
                api(projects.stokDslModel)

                implementation(libs.kotlinx.coroutines.core)

                implementation(libs.ktor.utils)
                implementation(libs.kotlin.reflect)
            }
        }
    }
}
