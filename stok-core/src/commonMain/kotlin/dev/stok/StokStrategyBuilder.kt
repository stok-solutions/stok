package dev.stok

import dev.stok.common.definition.LibraryApi

@LibraryApi
interface StokStrategyBuilder {

    fun build(): StokStrategy
}
