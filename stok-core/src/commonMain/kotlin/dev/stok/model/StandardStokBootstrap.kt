package dev.stok.model

import dev.stok.MutableStokBootstrap
import dev.stok.integration.BootstrapStandardNames
import dev.stok.stereotype.MutableNamedValueContainer
import dev.stok.stereotype.NamedValueContainer
import dev.stok.stereotype.StatefulElement
import io.github.oshai.kotlinlogging.KLogger
import kotlinx.datetime.Clock

class StandardStokBootstrap : MutableStokBootstrap, MutableNamedValueContainer<Any> by MapBasedNamedValueContainer() {

    override val logger: KLogger get() = getByName(BootstrapStandardNames.logger)

    override val clock: Clock get() = getByName(BootstrapStandardNames.clock)

    override val parameters: NamedValueContainer<Any> get() = getByName(BootstrapStandardNames.parametersContainer)

    override val variables: MutableNamedValueContainer<Any> get() = getByName(BootstrapStandardNames.variablesContainer)

    override fun clone(): StandardStokBootstrap {
        val newBootstrap = StandardStokBootstrap()

        for ((key, element) in this@StandardStokBootstrap.toMap()) {
            newBootstrap[key] = when (element) {
                is StatefulElement<*> -> element.clone()
                else -> element
            }
        }

        return newBootstrap
    }
}
