package dev.stok.model

import dev.stok.StokStrategy
import dev.stok.StokStrategyManager
import dev.stok.common.definition.InternalLibraryApi
import dev.stok.common.util.uncheckedCast
import dev.stok.dsl.context.LifecycleEventContext
import dev.stok.dsl.context.StrategyContext
import dev.stok.dsl.context.StrategyEventContext
import dev.stok.integration.event.LifecycleEvent
import dev.stok.stereotype.StatefulElement
import dev.stok.stereotype.StokContext
import dev.stok.stereotype.StokEvent
import dev.stok.stereotype.StokTrigger
import dev.stok.trigger.ConditionalTrigger
import dev.stok.trigger.EventTrigger
import dev.stok.trigger.PeriodicMomentTrigger
import dev.stok.trigger.PeriodicTrigger
import dev.stok.trigger.StateChangeEventTrigger
import dev.stok.util.randomId
import io.github.oshai.kotlinlogging.KLogger
import io.ktor.util.collections.ConcurrentMap
import kotlinx.datetime.Instant

@InternalLibraryApi
internal class StandardStokStrategy : StokStrategy {

    override val triggers: Collection<StokTrigger<*>> get() = triggerById.values

    private val triggerById: MutableMap<String, StokTrigger<*>> = ConcurrentMap()

    override fun registerTrigger(trigger: StokTrigger<*>): TriggerRegistration {
        val registrationId = randomId()
        triggerById[registrationId] = trigger

        LOGGER.debug { "Registered trigger [$trigger] by id [$registrationId]" }
        return TriggerRegistration(registrationId)
    }

    override fun unregisterTrigger(registration: TriggerRegistration): Boolean {
        val removed = (triggerById.remove(registration.id) != null)
        if (!removed) {
            LOGGER.warn { "Could not find trigger registration with id [${registration.id}]" }
        } else {
            LOGGER.debug { "Removed trigger registration with id [${registration.id}]" }
        }
        return removed
    }

    override suspend fun handleEvents(manager: StokStrategyManager, events: List<StokEvent>) {
        val currentTime: Instant = manager.bootstrap.clock.now()
            .also { LOGGER.trace { "Computed current time: $it" } }

        try {
            val sortedEvents = events.sortedWith(PRIORITY_EVENT_HANDLING_COMPARATOR)
            for (event in sortedEvents) {
                handleEvent(manager, event, currentTime)
            }
            LOGGER.trace { "Successfully handled [${events.size}] events" }
        } catch (ex: Throwable) {
            LOGGER.error(ex) { "Error happened during handling events batch" }
        }
    }

    override fun clone(): StandardStokStrategy =
        StandardStokStrategy().also { newStrategy ->
            for ((id, trigger) in triggerById) {
                newStrategy.triggerById[id] = when (trigger) {
                    is StatefulElement<*> -> uncheckedCast(trigger.clone())
                    else -> trigger
                }
            }
        }

    private suspend fun handleEvent(
        manager: StokStrategyManager,
        event: StokEvent,
        currentTime: Instant
    ) {
        for (rawTrigger: StokTrigger<*> in triggers) {
            val trigger: StokTrigger<StokContext> = uncheckedCast(rawTrigger)
            val context: StokContext = buildContextForTrigger(trigger, event, manager, currentTime)

            val isTriggered = trigger.isTriggered(context)
            if (isTriggered) {
                try {
                    trigger.trigger(context)
                    LOGGER.trace { "Successfully triggered [$trigger] for event [$event]" }
                } catch (ex: Throwable) {
                    LOGGER.error(ex) { "Processing of trigger [$trigger] failed for event [$event]" }
                }
            }
        }
    }

    private fun buildContextForTrigger(
        trigger: StokTrigger<*>,
        event: StokEvent,
        manager: StokStrategyManager,
        currentTime: Instant
    ): StokContext {
        fun illegalEventState(): Nothing = error("Illegal state for event [${event::class.simpleName}]")

        return when (event) {
            is LifecycleEvent.StateChangeEvent -> when (trigger) {
                is StateChangeEventTrigger<*> -> LifecycleEventContext(currentTime, manager.bootstrap, event)
                else -> illegalEventState()
            }
            is LifecycleEvent.InterruptEvent -> when (trigger) {
                is PeriodicTrigger -> StrategyContext(currentTime, manager)
                is PeriodicMomentTrigger -> StrategyContext(currentTime, manager)
                is ConditionalTrigger -> StrategyContext(currentTime, manager)
                else -> illegalEventState()
            }
            else -> when (trigger) {
                is EventTrigger<*> -> StrategyEventContext(currentTime, manager, event)
                else -> illegalEventState()
            }
        }
    }

    private companion object {

        @Suppress("MagicNumber")
        private val PRIORITY_EVENT_HANDLING_COMPARATOR: Comparator<StokEvent> = compareBy { event ->
            // The less value — the higher priority
            when (event) {
                is LifecycleEvent.StateChangeEvent -> 10
                is LifecycleEvent.InterruptEvent -> 100
                else -> 50
            }
        }

        private val LOGGER: KLogger = StokStrategy.LOGGER
    }
}
