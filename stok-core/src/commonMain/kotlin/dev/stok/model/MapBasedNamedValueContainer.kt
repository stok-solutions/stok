package dev.stok.model

import dev.stok.common.util.uncheckedCast
import dev.stok.stereotype.MutableNamedValueContainer
import dev.stok.stereotype.StatefulElement
import io.ktor.util.collections.ConcurrentMap

class MapBasedNamedValueContainer<T>(
    initialValues: Map<String, T> = emptyMap()
) : MutableNamedValueContainer<T>, StatefulElement<MapBasedNamedValueContainer<T>> {

    private val values: MutableMap<String, T> = ConcurrentMap()

    init {
        values.putAll(initialValues)
    }

    override fun <V : T> getByName(name: String): V =
        uncheckedCast(values.getValue(name))

    override fun <V : T> setByName(name: String, value: V) {
        values[name] = value
    }

    override fun hasByName(name: String): Boolean =
        name in values

    override fun toMap(): Map<String, T> = values.toMutableMap()

    override fun clone(): MapBasedNamedValueContainer<T> =
        MapBasedNamedValueContainer(
            initialValues = toMap().mapValues { (_, value) ->
                when (value) {
                    null -> null
                    is StatefulElement<*> -> value.clone()
                    else -> value
                }.let(::uncheckedCast)
            },
        )
}
