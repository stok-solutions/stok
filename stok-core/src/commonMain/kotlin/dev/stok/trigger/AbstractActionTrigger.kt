package dev.stok.trigger

import dev.stok.context.TimeAwareContext
import dev.stok.dsl.stereotype.DslAction
import dev.stok.stereotype.ActivationTimeAware
import dev.stok.stereotype.StokContext
import dev.stok.stereotype.StokTrigger
import dev.stok.stereotype.computeOn
import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.datetime.Instant
import kotlin.reflect.KClass

abstract class AbstractActionTrigger<in C : StokContext>(
    protected val action: DslAction<C>,
    private val contextClass: KClass<in C>
) : StokTrigger<C> {

    protected val LOGGER: KLogger = KotlinLogging.logger(name = this::class.qualifiedName!!)

    final override suspend fun trigger(context: C) {
        if (this is ActivationTimeAware) {
            check(context is TimeAwareContext) { "[ActivationAwareTrigger] must be provided with [TimeAwareContext]" }

            val currentTime = context.currentTime
            val nextActivationTime = nextActivationTime(context)
            if ((nextActivationTime != Instant.DISTANT_FUTURE) && currentTime != nextActivationTime) {
                LOGGER.warn { "[${this::class.simpleName}] is activated out of activation time window" }
            }
        }

        try {
            action.computeOn(context)
            afterSuccessfulTrigger(context)
        } finally {
            afterTrigger(context)
        }
    }

    protected open suspend fun afterSuccessfulTrigger(context: C) {
        // Nothing to do
    }

    protected open suspend fun afterTrigger(context: C) {
        // Nothing to do
    }

    override fun isTriggered(context: StokContext): Boolean {
        if (!contextClass.isInstance(context)) {
            LOGGER.warn {
                "Incorrect context class [${context::class.simpleName}]" +
                    " was provided to trigger [${this::class.simpleName}]." +
                    " Expected context class [${contextClass.simpleName}]"
            }
            return false
        }

        if (this is ActivationTimeAware) {
            check(context is TimeAwareContext) { "[ActivationAwareTrigger] must be provided with [TimeAwareContext]" }

            val nextActivationTime = nextActivationTime(context)
            if (nextActivationTime != Instant.DISTANT_FUTURE) {
                return (context.currentTime == nextActivationTime)
            }
        }

        return true
    }
}
