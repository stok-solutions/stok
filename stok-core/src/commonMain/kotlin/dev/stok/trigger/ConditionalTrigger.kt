package dev.stok.trigger

import dev.stok.common.util.uncheckedCast
import dev.stok.context.TimeAwareContext
import dev.stok.dsl.context.StrategyContext
import dev.stok.dsl.stereotype.DslAction
import dev.stok.dsl.stereotype.StokCondition
import dev.stok.stereotype.ActivationTimeAware
import dev.stok.stereotype.StokContext
import kotlinx.datetime.Instant

/**
 * @see dev.stok.dsl.context.StrategyBuilderContext.onCondition
 */
class ConditionalTrigger(
    val condition: StokCondition,
    action: DslAction<StrategyContext>
) : AbstractActionTrigger<StrategyContext>(action, StrategyContext::class), ActivationTimeAware {

    override fun isTriggered(context: StokContext): Boolean =
        super.isTriggered(context) && isTriggered(uncheckedCast(context))

    private fun isTriggered(context: StrategyContext): Boolean =
        condition.isSatisfied(context)

    override fun nextActivationTime(context: TimeAwareContext): Instant =
        if (condition is ActivationTimeAware) {
            condition.nextActivationTime(context)
        } else {
            Instant.DISTANT_FUTURE
        }
}
