package dev.stok.trigger

import dev.stok.common.util.uncheckedCast
import dev.stok.dsl.context.LifecycleEventContext
import dev.stok.dsl.stereotype.DslAction
import dev.stok.enum.StrategyState
import dev.stok.integration.event.LifecycleEvent.AfterStateChangeEvent
import dev.stok.integration.event.LifecycleEvent.BeforeStateChangeEvent
import dev.stok.integration.event.LifecycleEvent.StateChangeEvent
import dev.stok.stereotype.StokContext

sealed class StateChangeEventTrigger<E : StateChangeEvent>(
    action: DslAction<LifecycleEventContext<E>>
) : AbstractActionTrigger<LifecycleEventContext<E>>(action, LifecycleEventContext::class) {

    override fun isTriggered(context: StokContext): Boolean {
        if (!super.isTriggered(context)) return false
        val context = uncheckedCast<LifecycleEventContext<*>>(context)

        val event = context.event
        if (event !is StateChangeEvent) return false

        return when (event) {
            is BeforeStateChangeEvent -> isTriggered(
                precedingState = event.precedingState,
                targetState = event.newState,
                beforeTransition = true,
            )

            is AfterStateChangeEvent -> isTriggered(
                precedingState = event.precedingState,
                targetState = event.newState,
                beforeTransition = false,
            )
        }
    }

    abstract fun isTriggered(
        precedingState: StrategyState,
        targetState: StrategyState,
        beforeTransition: Boolean
    ): Boolean
}

/**
 * @see dev.stok.dsl.context.LifecycleBuilderContext.beforeStart
 */
class BeforeStartTransitionTrigger(
    action: DslAction<LifecycleEventContext<BeforeStateChangeEvent>>
) : StateChangeEventTrigger<BeforeStateChangeEvent>(action) {

    override fun isTriggered(
        precedingState: StrategyState,
        targetState: StrategyState,
        beforeTransition: Boolean
    ): Boolean =
        beforeTransition &&
            (precedingState in setOf(StrategyState.CREATED, StrategyState.SCHEDULED)) &&
            (targetState == StrategyState.PARKED)
}

/**
 * @see dev.stok.dsl.context.LifecycleBuilderContext.afterStart
 */
class AfterStartTransitionTrigger(
    action: DslAction<LifecycleEventContext<AfterStateChangeEvent>>
) : StateChangeEventTrigger<AfterStateChangeEvent>(action) {

    override fun isTriggered(
        precedingState: StrategyState,
        targetState: StrategyState,
        beforeTransition: Boolean
    ): Boolean =
        !beforeTransition &&
            (precedingState in setOf(StrategyState.CREATED, StrategyState.SCHEDULED)) &&
            (targetState == StrategyState.PARKED)
}

/**
 * @see dev.stok.dsl.context.LifecycleBuilderContext.beforePause
 */
class BeforePauseTransitionTrigger(
    action: DslAction<LifecycleEventContext<BeforeStateChangeEvent>>
) : StateChangeEventTrigger<BeforeStateChangeEvent>(action) {

    override fun isTriggered(
        precedingState: StrategyState,
        targetState: StrategyState,
        beforeTransition: Boolean
    ): Boolean =
        beforeTransition && (targetState == StrategyState.PAUSED)
}

/**
 * @see dev.stok.dsl.context.LifecycleBuilderContext.afterPause
 */
class AfterPauseTransitionTrigger(
    action: DslAction<LifecycleEventContext<AfterStateChangeEvent>>
) : StateChangeEventTrigger<AfterStateChangeEvent>(action) {

    override fun isTriggered(
        precedingState: StrategyState,
        targetState: StrategyState,
        beforeTransition: Boolean
    ): Boolean =
        !beforeTransition && (targetState == StrategyState.PAUSED)
}

/**
 * @see dev.stok.dsl.context.LifecycleBuilderContext.beforeResume
 */
class BeforeResumeTransitionTrigger(
    action: DslAction<LifecycleEventContext<BeforeStateChangeEvent>>
) : StateChangeEventTrigger<BeforeStateChangeEvent>(action) {

    override fun isTriggered(
        precedingState: StrategyState,
        targetState: StrategyState,
        beforeTransition: Boolean
    ): Boolean =
        beforeTransition &&
            (precedingState == StrategyState.PAUSED) &&
            (targetState == StrategyState.PARKED)
}

/**
 * @see dev.stok.dsl.context.LifecycleBuilderContext.afterResume
 */
class AfterResumeTransitionTrigger(
    action: DslAction<LifecycleEventContext<AfterStateChangeEvent>>
) : StateChangeEventTrigger<AfterStateChangeEvent>(action) {

    override fun isTriggered(
        precedingState: StrategyState,
        targetState: StrategyState,
        beforeTransition: Boolean
    ): Boolean =
        !beforeTransition &&
            (precedingState == StrategyState.PAUSED) &&
            (targetState == StrategyState.PARKED)
}

/**
 * @see dev.stok.dsl.context.LifecycleBuilderContext.beforeInterrupt
 */
class BeforeInterruptTransitionTrigger(
    action: DslAction<LifecycleEventContext<BeforeStateChangeEvent>>
) : StateChangeEventTrigger<BeforeStateChangeEvent>(action) {

    override fun isTriggered(
        precedingState: StrategyState,
        targetState: StrategyState,
        beforeTransition: Boolean
    ): Boolean =
        beforeTransition && (targetState == StrategyState.RUNNING)
}

/**
 * @see dev.stok.dsl.context.LifecycleBuilderContext.afterInterrupt
 */
class AfterInterruptTransitionTrigger(
    action: DslAction<LifecycleEventContext<AfterStateChangeEvent>>
) : StateChangeEventTrigger<AfterStateChangeEvent>(action) {

    override fun isTriggered(
        precedingState: StrategyState,
        targetState: StrategyState,
        beforeTransition: Boolean
    ): Boolean =
        !beforeTransition && (targetState == StrategyState.RUNNING)
}

/**
 * @see dev.stok.dsl.context.LifecycleBuilderContext.beforeFinish
 */
class BeforeFinishTransitionTrigger(
    action: DslAction<LifecycleEventContext<BeforeStateChangeEvent>>
) : StateChangeEventTrigger<BeforeStateChangeEvent>(action) {

    override fun isTriggered(
        precedingState: StrategyState,
        targetState: StrategyState,
        beforeTransition: Boolean
    ): Boolean =
        beforeTransition && (targetState == StrategyState.FINISHING)
}

/**
 * @see dev.stok.dsl.context.LifecycleBuilderContext.afterFinish
 */
class AfterFinishTransitionTrigger(
    action: DslAction<LifecycleEventContext<AfterStateChangeEvent>>
) : StateChangeEventTrigger<AfterStateChangeEvent>(action) {

    override fun isTriggered(
        precedingState: StrategyState,
        targetState: StrategyState,
        beforeTransition: Boolean
    ): Boolean =
        !beforeTransition && (targetState == StrategyState.FINISHING)
}
