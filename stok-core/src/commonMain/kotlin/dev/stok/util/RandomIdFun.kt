package dev.stok.util

import kotlin.random.Random

private val DIGITS: CharArray = CharArray(size = 10, Int::digitToChar)
private val LOWERCASE_LETTERS: CharArray = CharArray(size = 'z' - 'a') { 'a' + it }
private val UPPERCASE_LETTERS: CharArray = CharArray(size = 'z' - 'a') { 'a' + it }

private val ID_ALPHABET: CharArray = LOWERCASE_LETTERS + UPPERCASE_LETTERS + DIGITS
private const val ID_DEFAULT_LENGTH: Int = 128
private val ID_RANDOM: Random = Random(seed = 42)

fun randomId(length: Int = ID_DEFAULT_LENGTH): String =
    buildString(capacity = length) {
        repeat(length) {
            append(ID_ALPHABET.random(random = ID_RANDOM))
        }
    }
