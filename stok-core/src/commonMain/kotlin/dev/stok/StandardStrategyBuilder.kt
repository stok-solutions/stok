package dev.stok

import dev.stok.common.definition.InternalLibraryApi
import dev.stok.dsl.context.LifecycleBuilderContext
import dev.stok.dsl.context.StrategyBuilderContext
import dev.stok.dsl.context.StrategyContext
import dev.stok.dsl.context.StrategyEventContext
import dev.stok.dsl.stereotype.DslAction
import dev.stok.dsl.stereotype.StokCondition
import dev.stok.dsl.typing.PeriodicMoment
import dev.stok.model.StandardStokStrategy
import dev.stok.model.TriggerRegistration
import dev.stok.stereotype.StokEvent
import dev.stok.trigger.ConditionalTrigger
import dev.stok.trigger.EventTrigger
import dev.stok.trigger.PeriodicMomentTrigger
import dev.stok.trigger.PeriodicTrigger
import kotlin.reflect.KClass
import kotlin.time.Duration

@InternalLibraryApi
internal class StandardStrategyBuilder : StokStrategyBuilder, StrategyBuilderContext {

    private val strategy: StandardStokStrategy = StandardStokStrategy()

    override fun build(): StandardStokStrategy = strategy

    override fun lifecycle(closure: LifecycleBuilderContext.() -> Unit) =
        closure.invoke(StandardLifecycleBuilder(strategy))

    override fun everyPeriod(
        period: Duration,
        triggerOnStart: Boolean,
        action: DslAction<StrategyContext>
    ): TriggerRegistration =
        strategy.registerTrigger(PeriodicTrigger(period = period, triggerOnStart = triggerOnStart, action = action))

    override fun atMoment(moment: PeriodicMoment, action: DslAction<StrategyContext>): TriggerRegistration =
        strategy.registerTrigger(PeriodicMomentTrigger(moment = moment, action = action))

    override fun onCondition(condition: StokCondition, action: DslAction<StrategyContext>): TriggerRegistration =
        strategy.registerTrigger(ConditionalTrigger(condition, action))

    override fun <E : StokEvent> onEvent(
        eventClass: KClass<E>,
        action: DslAction<StrategyEventContext<E>>
    ): TriggerRegistration =
        strategy.registerTrigger(EventTrigger(eventClass, action))
}
