package dev.stok.dsl

import dev.stok.StandardBootstrapBuilder
import dev.stok.StokBootstrap
import dev.stok.StokBootstrapBuilder
import dev.stok.integration.context.BootstrapBuilderContext

fun bootstrap(closure: BootstrapBuilderContext.() -> Unit): StokBootstrap {
    val builder: BootstrapBuilderContext = StandardBootstrapBuilder()

    closure.invoke(builder)

    val bootstrap: StokBootstrap = (builder as StokBootstrapBuilder).build()

    return bootstrap
}
