package dev.stok.dsl

import dev.stok.StokBootstrap
import dev.stok.StokStrategy
import dev.stok.StokStrategyManager
import dev.stok.integration.context.BootstrapBuilderContext
import dev.stok.model.StandardStokStrategyManager

infix fun StokStrategy.manage(closure: BootstrapBuilderContext.() -> Unit): StokStrategyManager =
    manageOn(bootstrap(closure))

infix fun StokStrategy.manageOn(bootstrap: StokBootstrap): StokStrategyManager =
    StandardStokStrategyManager(bootstrap = bootstrap, strategy = this)
