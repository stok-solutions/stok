package dev.stok.dsl

import dev.stok.StandardStrategyBuilder
import dev.stok.StokStrategy
import dev.stok.StokStrategyBuilder
import dev.stok.dsl.context.StrategyBuilderContext

fun strategy(closure: StrategyBuilderContext.() -> Unit): StokStrategy {
    val builder = StandardStrategyBuilder()
    closure.invoke(builder)

    val strategy: StokStrategy = (builder as StokStrategyBuilder).build()

    return strategy
}
