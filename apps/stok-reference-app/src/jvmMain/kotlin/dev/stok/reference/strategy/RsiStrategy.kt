package dev.stok.reference.strategy

import dev.stok.CandleIntervals
import dev.stok.StokStrategy
import dev.stok.common.kotlin.extension.times
import dev.stok.context.ParametersAwareContext
import dev.stok.context.VariablesAwareContext
import dev.stok.dsl.context.StrategyContext
import dev.stok.dsl.parameter
import dev.stok.dsl.strategy
import dev.stok.dsl.variable
import dev.stok.indicator.getCurrent
import dev.stok.integration.price
import dev.stok.integration.rsi
import dev.stok.model.StokAccount
import dev.stok.reference.condition.MarketClosingSoon
import dev.stok.reference.enum.MarketType.MOEX
import dev.stok.typing.InstrumentId
import kotlin.time.Duration.Companion.minutes

private val ParametersAwareContext.instrument: InstrumentId by parameter("instrumentId")
private val ParametersAwareContext.buyRatio: Double by parameter("buyRatio")

private var VariablesAwareContext.positionOpen: Boolean by variable("positionOpened", false)

val rsiStrategy: StokStrategy = strategy {
    everyPeriod(1.minutes, triggerOnStart = true) {
        if (shouldBuy(instrument)) {
            buyInstrument(instrument)
        }
    }
    everyPeriod(5.minutes) {
        if (shouldSell(instrument)) {
            sellInstrument(instrument)
        }
    }
    onCondition(MarketClosingSoon(MOEX)) {
        sellInstrument(instrument)
    }
}

context(StrategyContext)
private suspend fun shouldBuy(instrumentId: InstrumentId): Boolean {
    if (positionOpen) return false

    val rsi = indicators.rsi(instrumentId, CandleIntervals.INTERVAL_1DAY)
    logger.info { "RSI: $rsi" }

    val rsiValue = rsi.getCurrent().value ?: return false
    return rsiValue >= 70
}

context(StrategyContext)
private suspend fun shouldSell(instrumentId: InstrumentId): Boolean {
    if (!positionOpen) return false

    val rsi = indicators.rsi(instrumentId, CandleIntervals.INTERVAL_1DAY)
    logger.info { "RSI: $rsi" }

    val rsiValue = rsi.getCurrent().value ?: return false
    return rsiValue <= 30
}

context(StrategyContext)
private suspend fun buyInstrument(instrumentId: InstrumentId) {
    if (positionOpen) return

    val currencyCode = instruments.getInstrumentInfo(instrumentId).currency
    val availableMoney = run {
        val money = portfolio.getMoney(currencyCode, account = mainAccount())
        money.amount * buyRatio
    }

    val currentPrice = indicators.price(instrumentId, CandleIntervals.INTERVAL_5MIN).getCurrent().value ?: return
    val quantityToBuy = (availableMoney / currentPrice).toDouble().toLong()

    broker.marketBuy(instrumentId = instrumentId, quantity = quantityToBuy, account = mainAccount())
    positionOpen = true
}

context(StrategyContext)
private suspend fun sellInstrument(instrumentId: InstrumentId) {
    val positions = portfolio.getPositions(mainAccount())

    val openedPosition = positions.firstOrNull { it.instrumentId == instrumentId }
    if (openedPosition == null) {
        positionOpen = false
        return
    }

    broker.marketSell(openedPosition.instrumentId, openedPosition.lots, account = mainAccount())
    positionOpen = false
}

context(StrategyContext)
private suspend fun mainAccount(): StokAccount = portfolio.getAccounts().first()
