package dev.stok.reference.strategy

import dev.stok.StokStrategy
import dev.stok.context.ParametersAwareContext
import dev.stok.dsl.onEvent
import dev.stok.dsl.parameter
import dev.stok.dsl.strategy
import dev.stok.dsl.typing.PeriodicMoment.Companion.minutely
import dev.stok.integration.event.QuoteApiEvent
import dev.stok.reference.condition.MarketClosingSoon
import dev.stok.reference.condition.MarketOpenedRecently
import dev.stok.reference.enum.MarketType
import dev.stok.reference.enum.MarketType.MOEX
import dev.stok.typing.InstrumentId
import kotlin.time.Duration.Companion.seconds

private val ParametersAwareContext.tradedInstruments: List<InstrumentId> by parameter("stok.tradedInstruments")

// @formatter:off
@Suppress("ktlint")
private val periodicStrategy: StokStrategy =
strategy {
    everyPeriod(10.seconds) { // DSL method
        val instrument = tradedInstruments.random()
        if (shouldBuy(instrument)) {
            buyInstrument(instrument)
        }
    }
}

@Suppress("ktlint")
private val periodicMomentStrategy: StokStrategy =
strategy {
    atMoment(minutely(atSecond = 15)) { // DSL method
        val instrument = tradedInstruments.random()
        if (shouldBuy(instrument)) {
            buyInstrument(instrument)
        }
    }
}

@Suppress("ktlint")
private val eventStrategy: StokStrategy =
strategy {
    onEvent<QuoteApiEvent> { // DSL method
        val instrument = tradedInstruments.random()
        if (shouldBuy(instrument)) {
            buyInstrument(instrument)
        }
    }
}

@Suppress("ktlint")
private val conditionStrategy: StokStrategy =
strategy {
    onCondition(MarketOpenedRecently(MOEX)) { // DSL method
        buyInstrument(tradedInstruments.random())
    }
    onCondition(MarketClosingSoon(MOEX)) { // DSL method
        sellEverything()
    }
}

@Suppress("ktlint")
private fun conditionFunStrategy(market: MarketType): StokStrategy =
strategy {
    onCondition(MarketOpenedRecently(market)) { // DSL method
        buyInstrument(tradedInstruments.random())
    }
    onCondition(MarketClosingSoon(market)) { // DSL method
        sellEverything()
    }
}
// @formatter:on

private fun shouldBuy(instrument: InstrumentId): Boolean = false

private fun buyInstrument(instrument: InstrumentId): Nothing = TODO("Not implemented")

private fun sellEverything(): Nothing = TODO("Not implemented")
