package dev.stok.reference

import dev.stok.CurrencyCodes
import dev.stok.StokBootstrap
import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.dsl.bootstrap
import dev.stok.integration.adHoc
import dev.stok.integration.backtesting
import dev.stok.integration.finnhub
import dev.stok.integration.localExecutor
import dev.stok.integration.tinvest
import dev.stok.typing.MoneyValue
import dev.stok.typing.Ticker
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant
import kotlin.io.path.Path

val finnhubBootstrap: StokBootstrap = bootstrap {
    localExecutor()
    adHoc()

    finnhub {
        apiKey(System.getenv("FINNHUB_API_KEY"))
    }

    parameters["stok.minPortfolioMoney"] = MoneyValue(amount = 1000.toDecimal(), CurrencyCodes.RUB)
    parameters["stok.tradedInstruments"] = listOf("MSFT").map(::Ticker)
}

val tinvestBootstrap: StokBootstrap = bootstrap {
    localExecutor()
    adHoc()

    tinvest {
        token(System.getenv("TINVEST_TOKEN"))
        enableTrading(true)
        enableReadonlyAccess(true)
    }

    parameters["stok.tradedInstruments"] = listOf("YNDX").map(::Ticker)
    parameters["stok.minPortfolioMoney"] = MoneyValue(amount = 1000.toDecimal(), CurrencyCodes.RUB)
}

val backtestingBootstrap: StokBootstrap = bootstrap {
    backtesting {
        startTime(LocalDateTime.parse("2022-01-03T07:00:00").toInstant(TimeZone.UTC))

        candlesFrom(
            path = Path(".data/YNDX_5min__82610_2021-05-04-21-00_2023-05-04-21-00.csv"),
            instrumentId = Ticker("YNDX"),
        )
    }

    parameters["stok.tradedInstruments"] = listOf("YNDX").map(::Ticker)
    parameters["stok.minPortfolioMoney"] = MoneyValue(amount = 1000.toDecimal(), CurrencyCodes.RUB)
}
