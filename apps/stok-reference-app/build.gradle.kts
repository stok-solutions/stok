plugins {
    application
    id(BuildPlugins.STOK_COMPONENT)
}

application {
    mainClass.set("dev.stok.reference.ReferenceApplicationKt")
}

kotlin {
    sourceSets {
        jvmMain {
            dependencies {
                implementation(projects.stokCore)
                implementation(projects.integrations.stokIntegrationFinnhub)
                implementation(projects.integrations.stokIntegrationYahoo)
                implementation(projects.integrations.stokIntegrationTinvest)
                implementation(projects.integrations.stokIntegrationAdhoc)

                implementation(projects.integrations.stokIntegrationBacktesting)
                implementation(projects.integrations.stokIntegrationLocalExecutor)
                implementation(projects.integrations.stokIntegrationStandardIndicators)

//                implementation("org.apache.logging.log4j:log4j-core:2.20.0")
                // https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-slf4j2-impl
                implementation("org.apache.logging.log4j:log4j-slf4j2-impl:2.20.0")
                // https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-core
                implementation("org.apache.logging.log4j:log4j-core:2.20.0")
                // https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-api
                implementation("org.apache.logging.log4j:log4j-api:2.20.0")

                // https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-slf4j-impl
                // https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-core
                // https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-api
                // https://mvnrepository.com/artifact/org.slf4j/slf4j-reload4j
//                implementation("org.slf4j:slf4j-reload4j:2.0.7")
            }
        }
    }
}
