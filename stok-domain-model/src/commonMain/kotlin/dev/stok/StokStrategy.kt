package dev.stok

import dev.stok.common.definition.InternalLibraryApi
import dev.stok.common.definition.LibraryApi
import dev.stok.model.TriggerRegistration
import dev.stok.stereotype.StatefulElement
import dev.stok.stereotype.StokEvent
import dev.stok.stereotype.StokTrigger
import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging

@LibraryApi
interface StokStrategy : StatefulElement<StokStrategy> {

    @InternalLibraryApi
    val triggers: Collection<StokTrigger<*>>

    @InternalLibraryApi
    fun registerTrigger(trigger: StokTrigger<*>): TriggerRegistration

    @InternalLibraryApi
    fun unregisterTrigger(registration: TriggerRegistration): Boolean

    @InternalLibraryApi
    suspend fun handleEvents(manager: StokStrategyManager, events: List<StokEvent>)

    companion object {

        val LOGGER: KLogger = KotlinLogging.logger {}
    }
}
