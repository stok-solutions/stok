package dev.stok.kotlin.extension

import dev.stok.stereotype.MutableNamedValueContainer
import dev.stok.stereotype.NamedValueContainer
import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

inline fun <reified T : Any?> NamedValueContainer<Any?>.named(name: String): ReadOnlyProperty<Nothing?, T> =
    ReadOnlyProperty { _, _ ->
        val container = this@named
        container.getByName(name)
    }

inline fun <reified T : Any?> MutableNamedValueContainer<Any?>.named(name: String): ReadWriteProperty<Nothing?, T> =
    object : ReadWriteProperty<Nothing?, T> {

        override fun getValue(thisRef: Nothing?, property: KProperty<*>): T {
            val container = this@named
            return container.getByName(name)
        }

        override fun setValue(thisRef: Nothing?, property: KProperty<*>, value: T) {
            val container = this@named
            return container.setByName(name, value)
        }
    }
