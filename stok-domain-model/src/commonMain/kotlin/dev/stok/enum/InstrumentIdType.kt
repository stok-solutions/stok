package dev.stok.enum

sealed interface InstrumentIdType {

    /**
     * @see dev.stok.typing.Ticker
     */
    object Ticker : InstrumentIdType

    /**
     * @see dev.stok.typing.Figi
     */
    object Figi : InstrumentIdType

    /**
     * @see dev.stok.typing.Isin
     */
    object Isin : InstrumentIdType

    data class Custom(val type: String) : InstrumentIdType
}
