package dev.stok.enum

enum class TradeDirection { BUY, SELL }
