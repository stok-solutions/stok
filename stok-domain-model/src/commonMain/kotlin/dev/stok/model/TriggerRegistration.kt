package dev.stok.model

import kotlin.jvm.JvmInline

@JvmInline
value class TriggerRegistration(val id: String)
