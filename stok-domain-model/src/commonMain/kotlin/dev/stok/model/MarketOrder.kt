package dev.stok.model

import dev.stok.enum.TradeDirection
import dev.stok.typing.InstrumentId
import dev.stok.typing.Quantity

class MarketOrder(
    direction: TradeDirection,
    instrumentId: InstrumentId,
    lots: Quantity,
    orderInfo: OrderInfo,
    executionInfo: OrderExecutionInfo
) : StokOrder(direction, instrumentId, lots, orderInfo, executionInfo)
