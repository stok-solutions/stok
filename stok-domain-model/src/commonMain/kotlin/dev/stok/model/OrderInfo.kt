package dev.stok.model

import kotlinx.datetime.Instant

data class OrderInfo(
    val id: String,
    val accountId: String,
    val creationDate: Instant
)
