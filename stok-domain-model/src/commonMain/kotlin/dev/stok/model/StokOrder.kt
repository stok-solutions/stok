package dev.stok.model

import dev.stok.enum.TradeDirection
import dev.stok.typing.InstrumentId
import dev.stok.typing.Quantity

sealed class StokOrder(
    val direction: TradeDirection,
    val instrumentId: InstrumentId,
    val lots: Quantity,
    val info: OrderInfo,
    val executionInfo: OrderExecutionInfo
) {

    fun isBuy(): Boolean = (direction == TradeDirection.BUY)

    fun isSell(): Boolean = (direction == TradeDirection.SELL)
}
