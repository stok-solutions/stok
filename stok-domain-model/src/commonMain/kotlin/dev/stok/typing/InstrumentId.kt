package dev.stok.typing

import dev.stok.enum.InstrumentIdType

sealed interface InstrumentId {

    val value: String

    val type: InstrumentIdType
}

/**
 * International Security Identification Number
 */
@JvmInline value class Isin(override val value: IsinValue) : InstrumentId {
    override val type: InstrumentIdType.Isin get() = InstrumentIdType.Isin
}
typealias IsinValue = String

/**
 * Financial Instrument Global Identifier
 */
@JvmInline value class Figi(override val value: FigiValue) : InstrumentId {
    override val type: InstrumentIdType.Figi get() = InstrumentIdType.Figi
}
typealias FigiValue = String

@JvmInline value class Ticker(override val value: TickerValue) : InstrumentId {
    override val type: InstrumentIdType.Ticker get() = InstrumentIdType.Ticker
}
typealias TickerValue = String

data class CustomInstrumentId(
    override val type: InstrumentIdType.Custom,
    override val value: String
) : InstrumentId
