package dev.stok.typing

/**
 * ISO code of currency
 *
 * See [Wikipedia. ISO 4217](https://en.wikipedia.org/wiki/ISO_4217)
 *
 * @see [dev.stok.CurrencyCodes]
 */
typealias CurrencyCode = String
