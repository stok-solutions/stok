package dev.stok.typing

import dev.stok.common.typing.DecimalValue
import dev.stok.context.TimeAwareContext
import kotlinx.datetime.Instant

sealed interface Candle {

    val open: DecimalValue
    val close: DecimalValue
    val low: DecimalValue
    val high: DecimalValue
    val volume: Quantity
    val interval: CandleInterval

    operator fun component1(): DecimalValue = open

    operator fun component2(): DecimalValue = close

    operator fun component3(): DecimalValue = low

    operator fun component4(): DecimalValue = high

    operator fun component5(): Quantity = volume

    operator fun component6(): CandleInterval = interval
}

sealed interface TimeAwareCandle : Candle {

    val openTime: Instant

    val closeTime: Instant get() = openTime + interval

    operator fun component7(): Instant = openTime

    operator fun component8(): Instant = closeTime
}

context(TimeAwareContext)
fun TimeAwareCandle.isClosed(): Boolean =
    currentTime >= closeTime

data class PriceCandle(
    override val open: DecimalValue,
    override val close: DecimalValue,
    override val low: DecimalValue,
    override val high: DecimalValue,
    override val volume: Quantity,
    override val interval: CandleInterval
) : Candle

data class HistoricCandle(
    override val open: DecimalValue,
    override val close: DecimalValue,
    override val low: DecimalValue,
    override val high: DecimalValue,
    override val volume: Quantity,
    override val interval: CandleInterval,
    override val openTime: Instant
) : TimeAwareCandle
