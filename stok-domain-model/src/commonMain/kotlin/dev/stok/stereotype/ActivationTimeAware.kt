package dev.stok.stereotype

import dev.stok.context.TimeAwareContext
import kotlinx.datetime.Instant

interface ActivationTimeAware {

    /**
     * Compute next activation time. Must return [currentTime] if activation should happen now.
     *
     * @return next activation time or [Instant.DISTANT_FUTURE] if can't compute
     */
    fun nextActivationTime(context: TimeAwareContext): Instant
}
