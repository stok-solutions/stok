package dev.stok.stereotype

import dev.stok.common.definition.LibraryApi

@LibraryApi
interface StokTrigger<in C : StokContext> {

    fun isTriggered(context: StokContext): Boolean

    suspend fun trigger(context: C)
}
