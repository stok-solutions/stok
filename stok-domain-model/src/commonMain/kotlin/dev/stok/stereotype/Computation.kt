package dev.stok.stereotype

import dev.stok.common.util.uncheckedCast

fun <T, C : StokContext> T.asComputation(): Computation<C, T> =
    CompletedComputation(this)

suspend fun <C : StokContext, R> Computation<C, R>.computeOn(context: C): R =
    with(context) {
        compute()
    }

fun interface Computation<in C : StokContext, out R> {

    context(C) suspend fun compute(): R
}

class CompletedComputation<in C : StokContext, out R>(private val result: R) : Computation<C, R> {

    context(C) override suspend fun compute(): R = result
}

class FunComputation<in C : StokContext, out R>(private val computeFn: (context: C) -> R) : Computation<C, R> {

    context(C) override suspend fun compute(): R =
        computeFn(uncheckedCast(this))
}

class ClosureComputation<in C : StokContext, out R>(private val closure: C.() -> R) : Computation<C, R> {

    context(C) override suspend fun compute(): R =
        closure(uncheckedCast(this))
}
