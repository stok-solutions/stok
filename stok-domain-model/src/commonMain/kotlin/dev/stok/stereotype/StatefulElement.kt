package dev.stok.stereotype

interface StatefulElement<out T : StatefulElement<T>> {

    /**
     * MUST return instance with its initial values
     */
    fun clone(): T
}
