package dev.stok

import dev.stok.common.definition.InternalLibraryApi
import dev.stok.stereotype.MutableNamedValueContainer

@InternalLibraryApi
interface MutableStokBootstrap : StokBootstrap, MutableNamedValueContainer<Any>
