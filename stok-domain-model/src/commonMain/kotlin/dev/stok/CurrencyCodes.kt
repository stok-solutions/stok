package dev.stok

import dev.stok.typing.CurrencyCode

/**
 * Standard currency codes
 *
 * See [IBAN. Country Currency Codes](https://www.iban.com/currency-codes)
 *
 * @constructor Create empty Standard currency codes
 */
object CurrencyCodes {

    const val CNY: CurrencyCode = "CNY"

    const val GBP: CurrencyCode = "GBP"

    const val EUR: CurrencyCode = "EUR"

    const val RUB: CurrencyCode = "RUB"

    const val USD: CurrencyCode = "USD"
}
