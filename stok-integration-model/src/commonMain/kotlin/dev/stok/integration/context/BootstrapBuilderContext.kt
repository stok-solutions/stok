package dev.stok.integration.context

import dev.stok.common.definition.LibraryApi
import dev.stok.integration.stereotype.BootstrapDslMarker
import dev.stok.stereotype.MutableNamedValueContainer

@[BootstrapDslMarker LibraryApi]
interface BootstrapBuilderContext : MutableNamedValueContainer<Any> {

    val parameters: MutableMap<String, Any>
}
