package dev.stok.integration.event

import dev.stok.enum.StrategyState
import dev.stok.stereotype.StokEvent
import kotlinx.datetime.Instant

sealed interface LifecycleEvent : StokEvent {

    class InterruptEvent : LifecycleEvent {

        override lateinit var creationTime: Instant

        override lateinit var dispatchTime: Instant

        override fun toString(): String = "InterruptEvent(" +
            "creationTime=${if (::creationTime.isInitialized) creationTime else null}," +
            "dispatchTime=${if (::dispatchTime.isInitialized) dispatchTime else null}" +
            ")"
    }

    sealed interface StateChangeEvent : LifecycleEvent {

        val precedingState: StrategyState

        val newState: StrategyState
    }

    data class BeforeStateChangeEvent(
        override val precedingState: StrategyState,
        override val newState: StrategyState
    ) : StateChangeEvent {

        override lateinit var creationTime: Instant

        override lateinit var dispatchTime: Instant
    }

    data class AfterStateChangeEvent(
        override val precedingState: StrategyState,
        override val newState: StrategyState
    ) : StateChangeEvent {

        override lateinit var creationTime: Instant

        override lateinit var dispatchTime: Instant
    }
}
