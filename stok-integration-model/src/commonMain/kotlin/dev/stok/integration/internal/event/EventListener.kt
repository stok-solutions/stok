package dev.stok.integration.internal.event

interface EventListener<E : Any> {

    fun registerEvents(events: List<E>)

    fun registerEvent(event: E) = registerEvents(listOf(event))

    fun addEventSource(source: EventSource<E>) =
        source.subscribeOnEvents(::registerEvent)

    fun addBatchEventSource(source: BatchEventSource<E>) =
        source.subscribeOnEvents(::registerEvents)
}
