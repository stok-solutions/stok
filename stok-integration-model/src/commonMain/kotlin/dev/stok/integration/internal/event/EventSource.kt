package dev.stok.integration.internal.event

import dev.stok.common.stereotype.Closeable

interface EventSource<out E : Any> : Closeable {

    fun subscribeOnEvents(handler: (E) -> Unit)
}
