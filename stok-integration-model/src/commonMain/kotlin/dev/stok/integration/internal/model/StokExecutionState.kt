package dev.stok.integration.internal.model

import dev.stok.common.stereotype.DistributedField
import dev.stok.enum.StrategyState

interface StokExecutionState {

    val stateField: DistributedField<StrategyState>
}
