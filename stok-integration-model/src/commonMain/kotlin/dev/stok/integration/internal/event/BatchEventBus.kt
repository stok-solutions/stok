package dev.stok.integration.internal.event

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch

class BatchEventBus<E : Any>(
    private val scope: CoroutineScope,
    private val beforeEmit: ((List<E>) -> Unit)? = null,
    private val beforeRegister: ((E) -> Unit)? = null
) : EventBus<E> {

    private val subscriptions: MutableList<suspend (List<E>) -> Unit> = mutableListOf()

    private val eventsChannel: Channel<E> = Channel()

    override fun registerEvents(events: List<E>) {
        scope.launch {
            for (event in events) {
                beforeRegister?.let { fn -> fn(event) }
                eventsChannel.send(event)
            }
        }
    }

    override fun emitEvents() {
        scope.launch {
            val eventsToHandle: MutableList<E> = mutableListOf()
            while (!eventsChannel.isEmpty) {
                eventsToHandle.add(eventsChannel.receive())
            }

            emitEvents(eventsToHandle.toList())
        }
    }

    private suspend fun emitEvents(events: List<E>) {
        if (events.isEmpty()) return

        beforeEmit?.let { fn -> fn(events) }
        for (eventsHandler: suspend (List<E>) -> Unit in subscriptions) {
            eventsHandler(events)
        }
    }

    override fun subscribeOnEvents(handler: suspend (List<E>) -> Unit) {
        subscriptions += handler
    }

    override fun close() {
        eventsChannel.close()
    }
}
