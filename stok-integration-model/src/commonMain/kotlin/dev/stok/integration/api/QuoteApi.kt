package dev.stok.integration.api

import dev.stok.enum.InstrumentIdType
import dev.stok.integration.model.CandleRequest
import dev.stok.integration.model.OrderBook
import dev.stok.integration.stereotype.StokApi
import dev.stok.typing.CandleInterval
import dev.stok.typing.InstrumentId
import dev.stok.typing.TimeAwareCandle
import kotlinx.datetime.Instant

interface QuoteApi : StokApi {

    val supportedInstrumentIdTypes: Set<InstrumentIdType>

    /**
     * The first candle will have value [TimeAwareCandle.openTime] >= [CandleRequest.from]
     * The last candle will have value [TimeAwareCandle.openTime] <= [CandleRequest.to]
     *
     * @param request
     * @return
     */
    suspend fun getCandles(request: CandleRequest): List<TimeAwareCandle>

    suspend fun getCandles(
        instrumentId: InstrumentId,
        interval: CandleInterval,
        from: Instant,
        to: Instant
    ): List<TimeAwareCandle> = getCandles(CandleRequest(instrumentId, interval, from, to))

    suspend fun getOrderBook(instrumentId: InstrumentId, depth: Short): OrderBook
}
