package dev.stok.integration.model

import dev.stok.typing.CurrencyCode
import dev.stok.typing.FigiValue
import dev.stok.typing.IsinValue
import dev.stok.typing.TickerValue

data class InstrumentInfo(
    val displayName: String,
    val currency: CurrencyCode,
    val exchange: String,
    val figi: FigiValue,
    val isin: IsinValue,
    val ticker: TickerValue
)
