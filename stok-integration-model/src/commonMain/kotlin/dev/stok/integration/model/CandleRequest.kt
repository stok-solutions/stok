package dev.stok.integration.model

import dev.stok.typing.CandleInterval
import dev.stok.typing.InstrumentId
import kotlinx.datetime.Instant

/**
 * Request candles for instrument [instrumentId]
 * with interval [interval]
 * and [dev.stok.typing.TimeAwareCandle.openTime] in range [[from], [to]]
 */
data class CandleRequest(
    val instrumentId: InstrumentId,
    val interval: CandleInterval,
    val from: Instant,
    val to: Instant
) : Comparable<CandleRequest> {

    @Suppress("ktlint:standard:argument-list-wrapping")
    override fun compareTo(other: CandleRequest): Int =
        compareValuesBy(
            this, other,
            { it.instrumentId::class.simpleName },
            { it.instrumentId.value },
            CandleRequest::interval,
            CandleRequest::from,
            { (it.to - it.from).unaryMinus() }, // Biggest ranges go first
        )
}
