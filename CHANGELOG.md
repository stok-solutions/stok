# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] - 2024-03-19

### Added

- Add new context sections: `portfolio`, `broker`, `indicators`, `instruments`, `quotes`
- Add new `lifecycle { ... }` DSL block to do actions on strategy lifecycle
- Add new `onEvent { ... }` DSL block to do actions on strategy events
- Add new `onCondition { ... }` DSL block to do actions on strategy conditions
- Add new `everyPeriod { ... }` DSL block to do actions periodically
- Add new `atMoment { ... }` DSL block to do actions at specific moments of time
- Add integration with Tinvest via `tinvest { ... }`
- Add integration with Finnhub Stock API via `finnhub { ... }`

[unreleased]: https://gitlab.com/stok-solutions/stok/-/compare/main...develop
[0.1.0]: https://gitlab.com/stok-solutions/stockpulse/-/tree/v0.1.0?ref_type=tags
