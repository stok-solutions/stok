package dev.stok.dsl.typing

import dev.stok.dsl.typing.PeriodicMoment.Companion.daily
import dev.stok.dsl.typing.PeriodicMoment.Companion.minutely
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.datatest.withData
import io.kotest.matchers.shouldBe
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.UtcOffset
import kotlinx.datetime.toInstant
import kotlin.time.Duration
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

class PeriodicMomentTests : ShouldSpec({

    fun doTest(moment: PeriodicMoment, now: Instant, expectedDurationUntil: Duration) =
        moment.computeUntilNextActivation(now) shouldBe expectedDurationUntil

    fun dateOf(isoString: String): Instant =
        LocalDateTime.parse(isoString).toInstant(UtcOffset.ZERO)

    data class TestCase(val moment: PeriodicMoment, val now: Instant, val durationUntil: Duration)

    val nameFn: (TestCase) -> String = { case ->
        buildString {
            append(case.moment)
                .append(".computeDurationUntilNext(")
                .append(case.now)
                .append(')')
            append(" == ")
            append(case.durationUntil)
        }
    }

    context("PeriodicMoment.daily()") {

        withData(
            nameFn = nameFn,
            listOf(
                TestCase(daily(atHour = 1), dateOf("2007-12-31T00:00:00"), 1.hours),
                TestCase(daily(atHour = 1), dateOf("2007-12-31T00:30:00"), 30.minutes),
                TestCase(daily(atHour = 1), dateOf("2007-12-31T00:59:55"), 5.seconds),
                TestCase(daily(atHour = 1), dateOf("2007-12-31T01:00:00"), 0.minutes),
                TestCase(daily(atHour = 1), dateOf("2007-12-31T01:00:05"), 1.days - 5.seconds),
                TestCase(daily(atHour = 1), dateOf("2007-12-31T01:30:00"), 1.days - 30.minutes),
                TestCase(daily(atHour = 23), dateOf("2007-12-31T23:59:59"), 23.hours + 1.seconds),
            ),
        ) { case -> doTest(case.moment, case.now, case.durationUntil) }
    }

    context("PeriodicMoment.minutely()") {

        withData(
            nameFn = nameFn,
            listOf(
                TestCase(minutely(atSecond = 0), dateOf("2007-12-31T00:00:00"), 0.seconds),
                TestCase(minutely(atSecond = 0), dateOf("2007-12-31T00:00:05"), 55.seconds),
                TestCase(minutely(atSecond = 0), dateOf("2007-12-31T00:00:30"), 30.seconds),
                TestCase(minutely(atSecond = 0), dateOf("2007-12-31T00:00:55"), 5.seconds),
                TestCase(minutely(atSecond = 0), dateOf("2007-12-31T00:00:59"), 1.seconds),
                TestCase(minutely(atSecond = 15), dateOf("2007-12-31T00:00:00"), 15.seconds),
                TestCase(minutely(atSecond = 15), dateOf("2007-12-31T00:00:15"), 0.seconds),
                TestCase(minutely(atSecond = 15), dateOf("2007-12-31T00:00:30"), 45.seconds),
                TestCase(minutely(atSecond = 45), dateOf("2007-12-31T00:00:00"), 45.seconds),
                TestCase(minutely(atSecond = 45), dateOf("2007-12-31T00:00:30"), 15.seconds),
                TestCase(minutely(atSecond = 45), dateOf("2007-12-31T00:00:45"), 0.seconds),
            ),
        ) { case -> doTest(case.moment, case.now, case.durationUntil) }
    }
})
