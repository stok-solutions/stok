package dev.stok.dsl.stereotype

import dev.stok.common.definition.LibraryApi
import dev.stok.stereotype.Computation
import dev.stok.stereotype.StokContext

@LibraryApi
fun interface DslAction<in C : StokContext> : Computation<C, Unit>
