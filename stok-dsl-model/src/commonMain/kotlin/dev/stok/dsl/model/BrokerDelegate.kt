package dev.stok.dsl.model

import dev.stok.StokBootstrap
import dev.stok.dsl.kotlin.extension.orderApi
import dev.stok.integration.api.OrderApi
import dev.stok.model.LimitOrder
import dev.stok.model.MarketOrder
import dev.stok.model.StokAccount
import dev.stok.typing.InstrumentId
import dev.stok.typing.MoneyValue
import dev.stok.typing.Quantity

class BrokerDelegate(private val bootstrap: StokBootstrap) {

    internal val orderApi: OrderApi by bootstrap::orderApi

    suspend fun marketBuy(
        instrumentId: InstrumentId,
        quantity: Quantity,
        account: StokAccount
    ): MarketOrder =
        orderApi.marketBuy(instrumentId, quantity, account)

    suspend fun marketSell(
        instrumentId: InstrumentId,
        quantity: Quantity,
        account: StokAccount
    ): MarketOrder =
        orderApi.marketSell(instrumentId, quantity, account)

    suspend fun limitBuy(
        instrumentId: InstrumentId,
        quantity: Quantity,
        price: MoneyValue,
        account: StokAccount
    ): LimitOrder =
        orderApi.limitBuy(instrumentId, quantity, price, account)

    suspend fun limitSell(
        instrumentId: InstrumentId,
        quantity: Quantity,
        price: MoneyValue,
        account: StokAccount
    ): LimitOrder =
        orderApi.limitSell(instrumentId, quantity, price, account)
}
