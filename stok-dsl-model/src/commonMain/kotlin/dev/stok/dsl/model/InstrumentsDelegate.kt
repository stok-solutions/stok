package dev.stok.dsl.model

import dev.stok.StokBootstrap
import dev.stok.dsl.kotlin.extension.instrumentApi
import dev.stok.integration.api.InstrumentApi
import dev.stok.integration.model.InstrumentInfo
import dev.stok.typing.InstrumentId

class InstrumentsDelegate(private val bootstrap: StokBootstrap) {

    internal val instrumentApi: InstrumentApi by bootstrap::instrumentApi

    suspend fun getInstrumentInfo(instrumentId: InstrumentId): InstrumentInfo =
        instrumentApi.getInstrumentInfo(instrumentId)
}
