package dev.stok.dsl.kotlin.extension

import dev.stok.StokBootstrap
import dev.stok.common.definition.InternalLibraryApi
import dev.stok.integration.BootstrapStandardNames
import dev.stok.integration.api.AccountApi
import dev.stok.integration.api.ExecutorApi
import dev.stok.integration.api.InstrumentApi
import dev.stok.integration.api.OrderApi
import dev.stok.integration.api.PortfolioApi
import dev.stok.integration.api.QuoteApi

private inline fun <reified T : Any> StokBootstrap.safeGetByName(name: String): T {
    check(hasByName(name)) { "Could not find instance of [${T::class.simpleName}] by name [$name]" }

    val result: T = getByName(name)
    StokBootstrap.LOGGER.trace { "Accessed instance of [${T::class.simpleName}] by name [$name]" }
    return result
}

@InternalLibraryApi
val StokBootstrap.accountApi: AccountApi
    get() = safeGetByName(BootstrapStandardNames.accountApi)

@InternalLibraryApi
val StokBootstrap.portfolioApi: PortfolioApi
    get() = safeGetByName(BootstrapStandardNames.portfolioApi)

@InternalLibraryApi
val StokBootstrap.instrumentApi: InstrumentApi
    get() = safeGetByName(BootstrapStandardNames.instrumentApi)

@InternalLibraryApi
val StokBootstrap.orderApi: OrderApi
    get() = safeGetByName(BootstrapStandardNames.orderApi)

@InternalLibraryApi
val StokBootstrap.executorApi: ExecutorApi
    get() = safeGetByName(BootstrapStandardNames.executorApi)

@InternalLibraryApi
val StokBootstrap.quoteApi: QuoteApi
    get() = safeGetByName(BootstrapStandardNames.quoteApi)
