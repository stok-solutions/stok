package dev.stok.dsl

import dev.stok.context.VariablesAwareContext
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun <T : Any> variable(name: String, initialValue: T? = null): ReadWriteProperty<VariablesAwareContext, T> =
    object : ReadWriteProperty<VariablesAwareContext, T> {
        override fun getValue(thisRef: VariablesAwareContext, property: KProperty<*>): T {
            val container = thisRef.variables
            if (name !in container) {
                container[name] = initialValue!!
            }
            return container.getByName(name)
        }

        override fun setValue(thisRef: VariablesAwareContext, property: KProperty<*>, value: T) {
            val container = thisRef.variables
            container.setByName(name, value)
        }
    }
