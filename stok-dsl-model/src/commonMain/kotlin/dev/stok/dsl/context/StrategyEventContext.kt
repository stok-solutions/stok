package dev.stok.dsl.context

import dev.stok.StokStrategyManager
import dev.stok.common.definition.LibraryApi
import dev.stok.context.EventAwareContext
import dev.stok.context.VariablesAwareContext
import dev.stok.dsl.stereotype.StrategyDslMarker
import dev.stok.stereotype.StokEvent
import kotlinx.datetime.Instant

@[StrategyDslMarker LibraryApi]
class StrategyEventContext<E : StokEvent>(
    currentTime: Instant,
    strategyManager: StokStrategyManager,
    override val event: E
) : StrategyContext(currentTime, strategyManager), EventAwareContext, VariablesAwareContext
