package dev.stok.dsl.context

import dev.stok.StokStrategyManager
import dev.stok.stereotype.StokContext

interface StrategyManagerAwareContext : StokContext {

    val strategyManager: StokStrategyManager
}
