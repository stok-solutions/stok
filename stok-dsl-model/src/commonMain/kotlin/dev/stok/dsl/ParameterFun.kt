package dev.stok.dsl

import dev.stok.context.ParametersAwareContext
import kotlin.properties.ReadOnlyProperty

fun <T : Any> parameter(name: String): ReadOnlyProperty<ParametersAwareContext, T> =
    ReadOnlyProperty { thisRef, _ ->
        val container = thisRef.parameters
        container.getByName(name)
    }
