package dev.stok.common.kotlinx.coroutines

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.framework.concurrency.eventually
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.seconds

class ScopeCounterTests : ShouldSpec({

    fun newScope() = CoroutineScope(Dispatchers.Default)

    suspend fun withScope(block: suspend (scope: CoroutineScope) -> Unit) {
        val scope: CoroutineScope = newScope()
        try {
            block(scope)
        } finally {
            if (scope.isActive) {
                scope.cancel()
            }
        }
    }

    should("counter can be incremented") {
        withScope { scope ->
            val counter = ScopeCounter(initialValue = 0L, scope = scope)

            repeat(100) {
                launch { counter.pushIncrement(1L) }
            }

            eventually(1.seconds) { counter.getValue() shouldBe 100L }
        }
    }

    should("counter cannot be changed after scope.cancel()") {
        withScope { scope ->
            val counter = ScopeCounter(initialValue = 0L, scope = scope)
            scope.cancel()
            shouldThrow<IllegalStateException> { counter.pushIncrement(1L) }
        }
    }

    should("counter cannot be changed after counter.close()") {
        withScope { scope ->
            val counter = ScopeCounter(initialValue = 0L, scope = scope)
            counter.close()
            shouldThrow<IllegalStateException> { counter.pushIncrement(1L) }
        }
    }

    should("counter can be gotten after scope.cancel()") {
        withScope { scope ->
            val counter = ScopeCounter(initialValue = 0L, scope = scope)
            counter.pushIncrement(1L)
            scope.cancel()
            counter.getValue() shouldBe 1L
        }
    }

    should("counter cannot be gotten after counter.close()") {
        withScope { scope ->
            val counter = ScopeCounter(initialValue = 0L, scope = scope)
            counter.close()
            shouldThrow<IllegalStateException> { counter.getValue() }
        }
    }
})
