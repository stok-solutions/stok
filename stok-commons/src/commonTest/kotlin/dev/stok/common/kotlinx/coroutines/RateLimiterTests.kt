package dev.stok.common.kotlinx.coroutines

import io.kotest.assertions.throwables.shouldNotThrow
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.core.test.TestScope
import io.kotest.framework.concurrency.continually
import io.kotest.framework.concurrency.eventually
import io.kotest.matchers.longs.shouldBeLessThanOrEqual
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.cancel
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeout
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlinx.datetime.asTimeSource
import kotlin.time.Duration
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds

class RateLimiterTests : ShouldSpec(
    {

        fun newScope() = CoroutineScope(Dispatchers.Default)

        suspend fun <T> withScope(block: suspend (scope: CoroutineScope) -> T): T {
            val scope: CoroutineScope = newScope()
            return try {
                block(scope)
            } finally {
                if (scope.isActive) {
                    scope.cancel()
                }
            }
        }

        context("ComputingRateLimiter") {

            should("not more than 5 permits given until refill time") {
                val clock = TestClock()
                val startTime = Clock.System.now().also(clock::setCurrentTime)

                val refillTime = startTime + 1.minutes
                val refillMark = clock.asTimeSource().markNow() + 1.seconds

                withScope { scope ->
                    val rateLimiter = scope.ComputingRateLimiter(limit = 5, refillMarkFn = { refillMark })

                    shouldNotThrow<TimeoutCancellationException> {
                        withTimeout(1.seconds) {
                            rateLimiter.takePermits(5)
                        }
                    }

                    shouldThrow<TimeoutCancellationException> {
                        withTimeout(1.seconds) {
                            rateLimiter.takePermits(5)
                        }
                    }

                    clock.setCurrentTime(refillTime)
                    shouldNotThrow<TimeoutCancellationException> {
                        withTimeout(1.seconds) {
                            rateLimiter.takePermits(5)
                        }
                    }
                }
            }
        }

        context("DiscreteRateLimiter") {
            suspend fun TestScope.testDiscreteRateLimiterPermits(
                limit: Int,
                period: Duration,
                jobsCount: Int = 10 * limit
            ) {
                withScope { scope ->
                    val counter = scope.Counter()
                    val latch = scope.CountDownLatch(jobsCount)

                    val rateLimiter: RateLimiter = scope.DiscreteRateLimiter(
                        limit = limit,
                        period = period,
                        filledInitially = false,
                    )

                    val jobs: List<Job> = (0 until jobsCount).map {
                        launch {
                            latch.countDown()
                            latch.await()

                            rateLimiter.takePermit()
                            counter.pushIncrement(1L)
                        }
                    }

                    latch.await()
                    continually(period) { counter.getValue() shouldBeLessThanOrEqual limit.toLong() }
                    jobs.forEach { if (!it.isCompleted) it.cancel() }
                }
            }

            should("not more than 10 permits given within 5 seconds") {
                testDiscreteRateLimiterPermits(limit = 10, period = 5.seconds)
            }

            should("give all permits eventually") {
                withScope { scope ->
                    val counter = scope.Counter()

                    val rateLimiter: RateLimiter = scope.DiscreteRateLimiter(
                        limit = 10,
                        period = 1.seconds,
                        filledInitially = false,
                    )

                    repeat(30) {
                        launch {
                            rateLimiter.takePermit()
                            counter.pushIncrement(1L)
                        }
                    }

                    eventually(3.seconds) { counter.getValue() shouldBe 30 }
                }
            }
        }
    },
)

internal class TestClock : Clock {

    private lateinit var currentTime: Instant

    override fun now(): Instant = currentTime

    fun setCurrentTime(value: Instant) {
        this.currentTime = value
    }

    fun isInitialized(): Boolean =
        ::currentTime.isInitialized
}
