package dev.stok.common.stereotype

import dev.stok.common.kotlin.extension.ifTrue

interface DistributedField<T> {

    val localValue: T

    val lock: DistributedLock

    suspend fun getActualValue(): T

    suspend fun setNewValue(newValue: T)

    fun subscribeOnChanges(handler: suspend (previousValue: T, currentValue: T) -> Unit)
}

suspend fun <T> DistributedField<T>.compareAndSet(expectedValue: T, newValue: T): Boolean {
    if (getActualValue() != expectedValue) return false // fast path
    return withLock { lockedValue ->
        (lockedValue != expectedValue)
            .ifTrue { setNewValue(newValue) }
    }
}

suspend inline fun <T, R> DistributedField<T>.withLock(block: (lockedValue: T) -> R): R =
    lock.withLock {
        val lockedValue = getActualValue()
        block(lockedValue)
    }
