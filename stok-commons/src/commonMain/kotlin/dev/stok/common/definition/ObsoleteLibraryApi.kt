package dev.stok.common.definition

/**
 * Marks declarations that are **obsolete** in some API, which means that the design of the corresponding
 * declarations has serious known flaws, and they will be redesigned in the future.
 *
 * Roughly speaking, these declarations will be deprecated in the future but there is no replacement for them yet,
 * so they cannot be deprecated right away.
 *
 * Developer **SHOULD** mark declaration with [ObsoleteLibraryApi] annotation
 * if declaration is obsolete but there is no replacement for it.
 *
 * Developer **MUST NOT** mark declaration with [kotlin.Deprecated] annotation
 * when it is marked with [ObsoleteLibraryApi] annotation.
 *
 * Developer **SHOULD NOT** apply [ObsoleteLibraryApi] annotation to functions, properties, etc. of a class
 * if [ObsoleteLibraryApi] annotation is applied to the class.
 *
 * @author Mikhail Gostev
 * @see kotlinx.coroutines.ObsoleteCoroutinesApi
 * @see kotlin.Deprecated
 */
@MustBeDocumented
@Retention(value = AnnotationRetention.BINARY)
@RequiresOptIn(level = RequiresOptIn.Level.WARNING)
annotation class ObsoleteLibraryApi
