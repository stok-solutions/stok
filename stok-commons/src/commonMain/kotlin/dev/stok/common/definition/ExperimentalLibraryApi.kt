package dev.stok.common.definition

/**
 * Marks declarations that are experimental features.
 *
 * Experimental feature has **no** backward compatibility guarantees, including both binary and source compatibility.
 * Its API and semantics can and will be changed in next releases.
 *
 * Experimental feature can be used to evaluate its real-world strengths and weaknesses, gather and provide feedback.
 * According to the feedback, feature will be refined on its road to stabilization and promotion to a stable API.
 *
 * The best way to speed up preview feature promotion is providing the feedback on the feature.
 *
 * Developer **MUST** mark declaration with [ExperimentalLibraryApi] annotation
 * if the declaration published as API but there is no backward compatibility for it (source or binary)
 *
 * Developer **SHOULD NOT** apply [ExperimentalLibraryApi] annotation to functions, properties, etc. of a class
 * if [ExperimentalLibraryApi] annotation is applied to the class.
 *
 * @author Mikhail Gostev
 * @see kotlinx.coroutines.ExperimentalCoroutinesApi
 */
@MustBeDocumented
@Retention(value = AnnotationRetention.BINARY)
@RequiresOptIn(
    level = RequiresOptIn.Level.WARNING,
    message = "This is an experimental API. It may be changed or removed in the future.",
)
@Target(
    AnnotationTarget.CLASS,
    AnnotationTarget.ANNOTATION_CLASS,
    AnnotationTarget.PROPERTY,
    AnnotationTarget.FIELD,
    AnnotationTarget.LOCAL_VARIABLE,
    AnnotationTarget.VALUE_PARAMETER,
    AnnotationTarget.CONSTRUCTOR,
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER,
    AnnotationTarget.TYPEALIAS,
)
annotation class ExperimentalLibraryApi
