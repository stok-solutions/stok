package dev.stok.common.definition

/**
 * Annotation [LibraryApi] is applied to source code to indicate that the given code is used as API
 * by other modules and backward compatibility **MUST** be supported on changing source codes
 * marked by annotation [LibraryApi]
 *
 * Developer **MAY** use [LibraryApi] annotation together with annotations:
 * - [kotlin.DslMarker];
 * - [kotlin.ExtensionFunctionType];
 * - [kotlin.PublishedApi];
 * - [kotlin.ParameterName].
 *
 * Developer **MUST NOT** break backward compatibility (source and binary)
 * of declarations marked with [LibraryApi] annotation.
 *
 * Developer **SHOULD NOT** apply [LibraryApi] annotation functions and properties of a class
 * if [LibraryApi] annotation is applied to the class.
 *
 * @author Mikhail Gostev
 * @see kotlin.DslMarker
 * @see kotlin.ExtensionFunctionType
 * @see kotlin.PublishedApi
 * @see kotlin.ParameterName
 */
@MustBeDocumented
@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION, AnnotationTarget.TYPEALIAS, AnnotationTarget.PROPERTY)
annotation class LibraryApi
