package dev.stok.common.util

@Suppress("UNCHECKED_CAST")
fun <T> uncheckedCast(value: Any?): T = (value as T)
