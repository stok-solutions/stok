package dev.stok.common.util

import dev.stok.common.kotlin.extension.ifFalse
import dev.stok.common.kotlin.extension.ifTrue
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.yield
import kotlin.concurrent.thread
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds

@PublishedApi
internal val DEFAULT_DELAY: Duration = 10.milliseconds

fun tickerFlow(
    initialDelay: Duration = Duration.ZERO,
    delay: Duration = DEFAULT_DELAY
): Flow<Unit> = channelFlow {
    delay(initialDelay)
    while (isActive && !isClosedForSend) {
        send(Unit)
        delay(delay)
    }
}

fun CoroutineScope.tickerChannel(
    initialDelay: Duration = Duration.ZERO,
    delay: Duration = DEFAULT_DELAY
): ReceiveChannel<Unit> {
    val channel = Channel<Unit>()

    launch {
        var firstRun = true
        while (isActive && !channel.isClosedForSend) {
            when {
                firstRun -> {
                    delay(initialDelay)
                    firstRun = false
                }
                else -> delay(delay)
            }

            channel.send(Unit)
        }
    }

    return channel
}

suspend inline fun delayUntilCondition(
    waitDelay: Duration = DEFAULT_DELAY,
    condition: () -> Boolean
) {
    while (true) {
        val conditionFollowed = condition()
        if (conditionFollowed) break

        yield()
        val conditionFollowedAfterYield = condition()
        if (conditionFollowedAfterYield) break

        delay(waitDelay)
    }
}

inline fun forkIf(condition: Boolean, crossinline block: suspend () -> Unit) {
    condition
        .ifTrue { fork(block) }
        .ifFalse { runBlocking { block() } }
}

inline fun fork(crossinline block: suspend () -> Unit) {
    thread { runBlocking { launch { block() } } }
}
