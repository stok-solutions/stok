package dev.stok.common.typing.impl

import dev.stok.common.typing.DecimalValue
import dev.stok.common.typing.DecimalValueCompanion

class DefaultDecimalValueCompanion : DecimalValueCompanion {

    override val ZERO: DecimalValue = DecimalValueImpl(value = 0)

    override val ONE: DecimalValue = DecimalValueImpl(value = 1)

    override val TEN: DecimalValue = DecimalValueImpl(value = 10)

    override fun valueOf(value: Number): DecimalValue = DecimalValueImpl(value)

    override fun valueOf(value: String): DecimalValue = DecimalValueImpl(value)
}
