package dev.stok.common.typing

import dev.stok.common.typing.impl.DefaultDecimalValueCompanion

@Suppress("TooManyFunctions")
interface DecimalValue : Comparable<Any> {

    fun sqrt(): DecimalValue

    fun toDouble(): Double

    operator fun unaryPlus(): DecimalValue

    operator fun unaryMinus(): DecimalValue

    operator fun plus(other: DecimalValue): DecimalValue

    operator fun minus(other: DecimalValue): DecimalValue

    operator fun times(other: DecimalValue): DecimalValue

    operator fun div(other: DecimalValue): DecimalValue

    override fun compareTo(other: Any): Int

    override fun equals(other: Any?): Boolean

    override fun hashCode(): Int

    override fun toString(): String

    companion object : DecimalValueCompanion by DefaultDecimalValueCompanion()
}
