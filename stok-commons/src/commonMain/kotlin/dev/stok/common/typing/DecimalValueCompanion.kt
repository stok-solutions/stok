package dev.stok.common.typing

interface DecimalValueCompanion {

    @Suppress("PropertyName")
    val ZERO: DecimalValue

    @Suppress("PropertyName")
    val ONE: DecimalValue

    @Suppress("PropertyName")
    val TEN: DecimalValue

    fun valueOf(value: Number): DecimalValue

    fun valueOf(value: String): DecimalValue
}
