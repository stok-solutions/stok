package dev.stok.common.kotlin.extension

import dev.stok.common.typing.DecimalValue
import dev.stok.common.util.identity

inline fun <T> Iterable<T>.sumOf(selector: (T) -> DecimalValue): DecimalValue {
    var sum: DecimalValue = 0.toDecimal()
    for (element in this) {
        sum += selector(element)
    }
    return sum
}

inline fun <T> Sequence<T>.sumOf(selector: (T) -> DecimalValue): DecimalValue {
    var sum: DecimalValue = 0.toDecimal()
    for (element in this) {
        sum += selector(element)
    }
    return sum
}

@Suppress("NOTHING_TO_INLINE")
inline fun Iterable<DecimalValue>.sum(): DecimalValue =
    sumOf(DecimalValue::identity)

@Suppress("NOTHING_TO_INLINE")
inline fun Sequence<DecimalValue>.sum(): DecimalValue =
    sumOf(DecimalValue::identity)
