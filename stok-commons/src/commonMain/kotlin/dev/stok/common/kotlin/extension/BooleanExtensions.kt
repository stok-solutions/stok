package dev.stok.common.kotlin.extension

inline fun Boolean.ifTrue(block: () -> Unit): Boolean =
    this.also { value -> if (value) block() }

inline fun Boolean.ifFalse(block: () -> Unit): Boolean =
    this.also { value -> if (value.not()) block() }

inline fun <R> Boolean.fold(
    crossinline ifTrue: () -> R,
    crossinline ifFalse: () -> R
): R = this.let { value ->
    when {
        value -> ifTrue()
        else -> ifFalse()
    }
}

inline fun <T> T.applyIf(
    crossinline condition: () -> Boolean,
    crossinline closure: T.() -> Unit
): T = apply { if (condition()) closure() }

inline fun <T> T.applyIf(
    condition: Boolean,
    crossinline closure: T.() -> Unit
): T = applyIf(condition = { condition }, closure = closure)
