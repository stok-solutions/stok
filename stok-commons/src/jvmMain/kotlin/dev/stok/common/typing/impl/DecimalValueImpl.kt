package dev.stok.common.typing.impl

import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.common.typing.DecimalValue
import dev.stok.common.util.uncheckedCast
import java.math.BigDecimal
import java.math.BigInteger
import java.math.MathContext

private fun DecimalValue.unsafe(): DecimalValueImpl = uncheckedCast(this)

actual class DecimalValueImpl : DecimalValue {

    private val value: BigDecimal

    internal constructor(value: BigDecimal) {
        this.value = value
    }

    actual constructor(value: Number) : this(
        when (value) {
            is Byte, is Short, is Int, is Long -> BigDecimal(value.toLong(), MathContext.DECIMAL128)
            is Float, is Double -> BigDecimal(value.toDouble(), MathContext.DECIMAL128)
            is BigInteger -> value.toBigDecimal()
            is BigDecimal -> value
            else -> error("Unsupported type of value [${value::class.qualifiedName}]")
        },
    )

    actual constructor(value: String) : this(BigDecimal(value, MathContext.DECIMAL128))

    actual override fun sqrt(): DecimalValue =
        DecimalValueImpl(value.sqrt(MathContext.DECIMAL128))

    actual override fun toDouble(): Double =
        value.toDouble()

    actual override operator fun unaryPlus(): DecimalValue =
        DecimalValueImpl(value.plus(MathContext.DECIMAL128))

    actual override operator fun unaryMinus(): DecimalValue =
        DecimalValueImpl(value.negate(MathContext.DECIMAL128))

    actual override operator fun plus(other: DecimalValue): DecimalValue =
        DecimalValueImpl(value.add(other.unsafe().value, MathContext.DECIMAL128))

    actual override operator fun minus(other: DecimalValue): DecimalValue =
        DecimalValueImpl(value.subtract(other.unsafe().value, MathContext.DECIMAL128))

    actual override operator fun times(other: DecimalValue): DecimalValue =
        DecimalValueImpl(value.multiply(other.unsafe().value, MathContext.DECIMAL128))

    actual override operator fun div(other: DecimalValue): DecimalValue =
        DecimalValueImpl(value.divide(other.unsafe().value, MathContext.DECIMAL128))

    actual override fun compareTo(other: Any): Int {
        val effectiveOther = when (other) {
            is DecimalValue -> other.unsafe()
            is Number -> other.toDecimal().unsafe()
            is String -> other.toDecimal().unsafe()
            else -> error("Unsupported comparison with instance of type [${other::class.simpleName}]")
        }
        return value.compareTo(effectiveOther.value)
    }

    actual override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DecimalValueImpl

        if (value.compareTo(other.value) != 0) return false

        return true
    }

    actual override fun hashCode(): Int =
        value.hashCode()

    actual override fun toString(): String =
        value.toEngineeringString()
}
