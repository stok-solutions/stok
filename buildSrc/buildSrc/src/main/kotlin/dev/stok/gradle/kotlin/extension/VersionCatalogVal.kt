package dev.stok.gradle.kotlin.extension

import org.gradle.api.Project
import org.gradle.api.artifacts.VersionCatalogsExtension
import org.gradle.api.provider.Provider
import org.gradle.kotlin.dsl.getByType

/*internal*/ val Project.versionCatalog: Provider<BootstrapVersionCatalog>
    get() = provider {
        rootProject.extensions
            .getByType<VersionCatalogsExtension>().single()
            .let(::BootstrapVersionCatalog)
    }
