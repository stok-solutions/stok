import dev.stok.gradle.kotlin.extension.stokVersion

if (project == rootProject) {
    plugins.apply(BuildPlugins.DOKKA)
}

version = stokVersion.get()

layout.buildDirectory.set(
    run {
        val globalBuildDir: File = rootProject.projectDir.resolve("build")
        when (project) {
            rootProject -> globalBuildDir.resolve(project.name)
            else -> {
                val relativeProjectPath = projectDir.relativeTo(rootProject.projectDir)
                globalBuildDir.resolve(relativeProjectPath)
            }
        }
    }
)
