plugins {
    id("stok.component")
    id("maven-publish")
    id("org.jetbrains.dokka")
    id("org.jetbrains.kotlinx.kover")
}

val javadocJar by tasks.registering(Jar::class) {
    archiveClassifier.set("javadoc")
}

fun getExtraString(name: String) = ext[name]?.toString()

publishing {
    // Configure all publications
    publications.withType<MavenPublication> {

        // Stub javadoc.jar artifact
        artifact(javadocJar.get())

        // Provide artifacts information requited by Maven Central
        pom {
            name.set("Stok")
            description.set("Asynchronous framework for creating trading strategies")
            url.set("https://gitlab.com/stok-solutions/stok")
            inceptionYear.set("2023")

            licenses {
                license {
                    name.set("Apache 2.0")
                    url.set("https://opensource.org/license/apache-2-0")
                }
            }
            developers {
                developer {
                    id.set("pihanya")
                    name.set("Mikhail Gostev")
                    email.set("gostev.mike@gmail.com")
                }
            }
            scm {
                url.set("https://gitlab.com/stok-solutions/stok")
            }
        }
    }

    repositories {
        val nexusUser = System.getProperty("stok.nexus.user")
        val nexusPassword = System.getProperty("stok.nexus.password")

        maven {
            name = "stok-hosted-maven-development"
            url = uri("https://nexus.stok.dev/repository/stok-hosted-maven-development/")
            credentials {
                username = nexusUser
                password = nexusPassword
            }
        }
    }
}
