plugins {
    `kotlin-dsl`
}

layout.buildDirectory.set(
    projectDir.parentFile
        .resolve("build")
        .resolve("buildSrc")
)

dependencies {
    implementation(libs.gradlePlugins.spotless)

    implementation(libs.gradlePlugins.dokka)
    implementation(libs.gradlePlugins.kotlinx.kover)

    implementation(libs.gradlePlugins.kotlin)
    implementation(libs.gradlePlugins.kotest.multiplatform)
}

kotlin {
    sourceSets.getByName("main").kotlin.srcDir("buildSrc/src/main/kotlin")
}
