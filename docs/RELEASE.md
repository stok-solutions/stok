# Stok Project Release Process

This guide describes how to release project version `x.y.z` (release date `YYYY-MM-DD`)
with the previous release version `a.b.c`
and next release version `d.e.f`.

1. Create and checkout branch `release/x.y.z`:

    ```shell
    git checkout -b release/x.y.z
    ```

2. Replace `[Unreleased]` with `[x.y.z]` and set release date to `YYYY-MM-DD` in [CHANGELOG.md](../CHANGELOG.md):

    ```md
    ## [x.y.z] - YYYY-MM-DD
    ```

3. Replace footer link `[unreleased]` with link assigned to `x.y.z` in [CHANGELOG.md](../CHANGELOG.md):

    ```md
    [x.y.z]: https://gitlab.com/stok-solutions/stok/-/compare/va.b.c...vx.y.z
    ```

4. Bump project version to `x.y.z` in [libs.versions.toml](../gradle/libs.versions.toml), [README.md](../README.md):

    ```toml
    stok = "x.y.z"
    ```

5. Create release commit:

    ```shell
    git commit -m 'chore: release Stok vx.y.z'
    ```

6. Create and merge MR from `release/x.y.z` into the branch `main`. Do not delete source branch, do not squash commits.

7. Checkout branch `main`, create tag `vx.y.z`, push it and return to branch `release/x.y.z`:

    ```shell
    git checkout main
    git pull
    git tag vx.y.z
    git push origin --tags
    git checkout release/x.y.z
    ```

8. Publish artifacts using `Publish Stok Artifacts` run configuration in IntelliJ IDEA or Gradle command:

    ```shell
    ./gradlew publish
    ```

9. Add `[Unreleased]` section into [CHANGELOG.md](../CHANGELOG.md)

    ```md
    ## [Unreleased]
    
    ## [x.y.z] - YYYY-MM-DD
    ```

10. Add footer link into [CHANGELOG.md](../CHANGELOG.md):

    ```md
    [unreleased]: https://gitlab.com/stok-solutions/stok/-/compare/main...develop
    ```

11. Bump project version to `d.e.f-SNAPSHOT` in [libs.versions.toml](../gradle/libs.versions.toml), [README.md](../README.md):

    ```toml
    stok = "d.e.f-SNAPSHOT"
    ```

12. Create a new commit:

    ```shell
    git commit -m 'chore: bump develop to d.e.f-SNAPSHOT'
    ```

13. Create and merge MR from `release/x.y.z` into the branch `develop`. Do not squash commits.
