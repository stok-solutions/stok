package dev.stok.integration.finnhub.api

import dev.stok.CandleIntervals
import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.enum.InstrumentIdType
import dev.stok.integration.api.QuoteApi
import dev.stok.integration.model.CandleRequest
import dev.stok.integration.model.OrderBook
import dev.stok.typing.CandleInterval
import dev.stok.typing.HistoricCandle
import dev.stok.typing.InstrumentId
import io.finnhub.api.apis.DefaultApi
import io.finnhub.api.infrastructure.ApiClient
import io.finnhub.api.models.StockCandles
import kotlinx.coroutines.runInterruptible
import kotlinx.datetime.Instant

class FinnhubQuoteApi(apiKey: String) : QuoteApi {

    override val supportedInstrumentIdTypes: Set<InstrumentIdType> = setOf(InstrumentIdType.Ticker)

    init {
        ApiClient.apiKey["token"] = apiKey
    }

    private val api: DefaultApi = DefaultApi()

    override suspend fun getCandles(request: CandleRequest): List<HistoricCandle> {
        val candles: StockCandles = runInterruptible {
            api.stockCandles(
                symbol = request.instrumentId.value,
                resolution = toCandleResolution(request.interval),
                from = request.from.epochSeconds,
                to = request.to.epochSeconds,
            )
        }

        if (candles.s == "no_data" || candles.t.isNullOrEmpty()) {
            return emptyList()
        }

        return buildList list@{
            for (index in candles.t.orEmpty().indices) {
                this@list += HistoricCandle(
                    open = candles.o!![index].toDecimal(),
                    close = candles.c!![index].toDecimal(),
                    low = candles.l!![index].toDecimal(),
                    high = candles.h!![index].toDecimal(),
                    volume = candles.v!![index].toLong(),
                    interval = request.interval,
                    openTime = Instant.fromEpochSeconds(candles.t!![index]),
                )
            }
        }
    }

    override suspend fun getOrderBook(instrumentId: InstrumentId, depth: Short): OrderBook {
        TODO("Not yet implemented")
    }

    private fun toCandleResolution(value: CandleInterval): String = when (value) {
        CandleIntervals.INTERVAL_1MIN -> "1"
        CandleIntervals.INTERVAL_5MIN -> "5"
        CandleIntervals.INTERVAL_15MIN -> "15"
        CandleIntervals.INTERVAL_30MIN -> "30"
        CandleIntervals.INTERVAL_1HOUR -> "60"
        CandleIntervals.INTERVAL_1DAY -> "D"
        CandleIntervals.INTERVAL_1WEEK -> "W"
        else -> error("Unsupported interval: $value")
    }
}
