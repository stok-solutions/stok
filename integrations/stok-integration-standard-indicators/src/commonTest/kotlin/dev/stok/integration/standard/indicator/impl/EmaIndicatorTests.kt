package dev.stok.integration.standard.indicator.impl

import dev.stok.CandleIntervals
import dev.stok.common.kotlin.extension.absolute
import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.common.typing.DecimalValue
import dev.stok.indicator.IndicatorValue.None
import dev.stok.indicator.IndicatorValue.Value
import dev.stok.integration.standard.indicator.util.AdHocValueSource
import dev.stok.typing.CandleInterval
import io.kotest.assertions.withClue
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldHaveAtLeastSize
import io.kotest.matchers.comparables.shouldBeLessThan
import io.kotest.matchers.types.shouldBeInstanceOf
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant

class EmaIndicatorTests : ShouldSpec({
    val from = LocalDateTime.parse("2007-12-30T00:00:00").toInstant(TimeZone.UTC)
    val interval = CandleIntervals.INTERVAL_5MIN
    val source = AdHocValueSource<DecimalValue>()

    suspend fun EmaIndicator.doTestLastValue(
        expected: DecimalValue,
        interval: CandleInterval,
        from: Instant,
        to: Instant,
        precision: DecimalValue = "0.01".toDecimal()
    ) {
        val indicator = this
        val values = indicator.getValues(interval, from, to)

        val result = values
            .shouldHaveAtLeastSize(1)
            .last()
            .shouldBeInstanceOf<Value<DecimalValue>>()
            .value

        withClue("$result - $expected < $precision") {
            (result - expected).absolute() shouldBeLessThan precision
        }
    }

    // https://school.stockcharts.com/doku.php?id=technical_indicators:moving_averages#ema_accuracy
    context("ema(10) plain tests") {
        val period = 10
        val indicator = EmaIndicator(source, period = period)

        val from = LocalDateTime.parse("2010-04-07T00:00:00").toInstant(TimeZone.UTC)
        val interval = CandleIntervals.INTERVAL_1DAY

        source.values = arrayOf(
            Value(from - (interval * 14), "22.2734".toDecimal()), // 24 Mar
            Value(from - (interval * 13), "22.194".toDecimal()), // 25 Mar
            Value(from - (interval * 12), "22.0847".toDecimal()), // 26 Mar
            None(from - (interval * 11)), // 27 Mar
            None(from - (interval * 10)), // 28 Mar

            Value(from - (interval * 9), "22.1741".toDecimal()), // 29 Mar
            Value(from - (interval * 8), "22.184".toDecimal()), // 30 Mar
            Value(from - (interval * 7), "22.1344".toDecimal()), // 31 Mar
            Value(from - (interval * 6), "22.2337".toDecimal()), // 1 Apr
            None(from - (interval * 5)), // 2 Apr
            None(from - (interval * 4)), // 3 Apr
            None(from - (interval * 3)), // 4 Apr

            Value(from - (interval * 2), "22.4323".toDecimal()), // 5 Apr
            Value(from - (interval * 1), "22.2436".toDecimal()), // 6 Apr
            Value(from + (interval * 0), "22.2933".toDecimal()), // 7 Apr
            Value(from + (interval * 1), "22.1542".toDecimal()), // 8 Apr
            Value(from + (interval * 2), "22.3926".toDecimal()), // 9 Apr
            None(from + (interval * 3)), // 10 Apr
            None(from + (interval * 4)), // 11 Apr

            Value(from + (interval * 5), "22.3816".toDecimal()), // 12 Apr
            Value(from + (interval * 6), "22.6109".toDecimal()), // 13 Apr
            Value(from + (interval * 7), "23.3558".toDecimal()), // 14 Apr
            Value(from + (interval * 8), "24.0519".toDecimal()), // 15 Apr
            Value(from + (interval * 9), "23.753".toDecimal()), // 16 Apr
            None(from + (interval * 10)), // 17 Apr
            None(from + (interval * 11)), // 18 Apr

            Value(from + (interval * 12), "23.8324".toDecimal()), // 19 Apr
            Value(from + (interval * 13), "23.9516".toDecimal()), // 20 Apr
            Value(from + (interval * 14), "23.6338".toDecimal()), // 21 Apr
            Value(from + (interval * 15), "23.8225".toDecimal()), // 22 Apr
            Value(from + (interval * 16), "23.8722".toDecimal()), // 23 Apr
            None(from + (interval * 17)), // 24 Apr
            None(from + (interval * 18)), // 25 Apr
        )

        should("7 Apr EMA equals 22.22 (SMA)") {
            indicator.doTestLastValue("22.22".toDecimal(), interval, from, from + interval * 0)
        }

        should("8 Apr EMA equals 22.21") {
            indicator.doTestLastValue("22.21".toDecimal(), interval, from, from + interval * 1)
        }

        should("9 Apr EMA equals 22.24") {
            indicator.doTestLastValue("22.24".toDecimal(), interval, from, from + interval * 2)
        }

        // TODO: Test `None` value
        // should("10 Apr EMA is None") {
        //
        // }

        // TODO: Test `None` value
        // should("11 Apr EMA is None") {
        //
        // }

        // TODO: Debug what is the problem
        xshould("12 Apr EMA equals 22.24") {
            indicator.doTestLastValue("22.24".toDecimal(), interval, from, from + interval * 5)
        }

        should("13 Apr EMA equals 22.33") {
            indicator.doTestLastValue("22.33".toDecimal(), interval, from, from + interval * 6)
        }

        should("14 Apr EMA equals 22.52") {
            indicator.doTestLastValue("22.52".toDecimal(), interval, from, from + interval * 7)
        }

        should("15 Apr EMA equals 22.80") {
            indicator.doTestLastValue("22.80".toDecimal(), interval, from, from + interval * 8)
        }
    }
})
