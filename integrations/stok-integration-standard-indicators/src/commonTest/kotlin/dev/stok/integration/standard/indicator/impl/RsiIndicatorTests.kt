package dev.stok.integration.standard.indicator.impl

import dev.stok.CandleIntervals
import dev.stok.common.kotlin.extension.absolute
import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.common.typing.DecimalValue
import dev.stok.indicator.IndicatorValue.None
import dev.stok.indicator.IndicatorValue.Value
import dev.stok.integration.standard.indicator.util.AdHocValueSource
import dev.stok.typing.CandleInterval
import io.kotest.assertions.withClue
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldEndWith
import io.kotest.matchers.collections.shouldHaveAtLeastSize
import io.kotest.matchers.comparables.shouldBeLessThan
import io.kotest.matchers.types.shouldBeInstanceOf
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant

class RsiIndicatorTests : ShouldSpec({
    val source = AdHocValueSource<DecimalValue>()

    suspend fun RsiIndicator.doTestLastValue(
        expected: DecimalValue,
        interval: CandleInterval,
        from: Instant,
        to: Instant,
        precision: DecimalValue = "0.01".toDecimal()
    ) {
        val indicator = this
        val values = indicator.getValues(interval, from, to)

        val result = values
            .shouldHaveAtLeastSize(1)
            .last()
            .shouldBeInstanceOf<Value<DecimalValue>>()
            .value

        withClue("|$result - $expected| < $precision") {
            (result - expected).absolute() shouldBeLessThan precision
        }
    }

    // See 'rsi14-plain-tests.xls' in test resources
    // See https://school.stockcharts.com/doku.php?id=technical_indicators:relative_strength_index_rsi#calculating_the_rsi
    context("rsi(14) plain tests") {
        val period = 14
        val indicator = RsiIndicator(source, period = period)

        val from = LocalDateTime.parse("2009-12-14T00:00:00").toInstant(TimeZone.UTC)
        val interval = CandleIntervals.INTERVAL_1DAY

        source.values = arrayOf(
            Value(from + (interval * 0), "44.3389".toDecimal()), // 14 Dec
            Value(from + (interval * 1), "44.0902".toDecimal()), // 15 Dec
            Value(from + (interval * 2), "44.1497".toDecimal()), // 16 Dec
            Value(from + (interval * 3), "43.6124".toDecimal()), // 17 Dec
            Value(from + (interval * 4), "44.3278".toDecimal()), // 18 Dec
            None(from + (interval * 5)), // 19 Dec
            None(from + (interval * 6)), // 20 Dec

            Value(from + (interval * 7), "44.8264".toDecimal()), // 21 Dec
            Value(from + (interval * 8), "45.0955".toDecimal()), // 22 Dec
            Value(from + (interval * 9), "45.4245".toDecimal()), // 23 Dec
            Value(from + (interval * 10), "45.8433".toDecimal()), // 24 Dec
            None(from + (interval * 11)), // 25 Dec
            None(from + (interval * 12)), // 26 Dec
            None(from + (interval * 13)), // 27 Dec

            Value(from + (interval * 14), "46.0826".toDecimal()), // 28 Dec
            Value(from + (interval * 15), "45.8931".toDecimal()), // 29 Dec
            Value(from + (interval * 16), "46.0328".toDecimal()), // 30 Dec
            Value(from + (interval * 17), "45.614".toDecimal()), // 31 Dec
            None(from + (interval * 18)), // 01 Jan
            None(from + (interval * 19)), // 02 Jan
            None(from + (interval * 20)), // 03 Jan

            Value(from + (interval * 21), "46.282".toDecimal()), // 04 Jan
            Value(from + (interval * 22), "46.282".toDecimal()), // 05 Jan
            Value(from + (interval * 23), "46.0028".toDecimal()), // 06 Jan
            Value(from + (interval * 24), "46.0328".toDecimal()), // 07 Jan
            Value(from + (interval * 25), "46.4116".toDecimal()), // 08 Jan
            None(from + (interval * 26)), // 09 Jan
            None(from + (interval * 27)), // 10 Jan

            Value(from + (interval * 28), "46.2222".toDecimal()), // 11 Jan
            Value(from + (interval * 29), "45.6439".toDecimal()), // 12 Jan
            Value(from + (interval * 30), "46.2122".toDecimal()), // 13 Jan
            Value(from + (interval * 31), "46.2521".toDecimal()), // 14 Jan
            Value(from + (interval * 32), "45.7137".toDecimal()), // 15 Jan
            None(from + (interval * 33)), // 16 Jan
            None(from + (interval * 34)), // 17 Jan
            None(from + (interval * 35)), // 18 Jan

            Value(from + (interval * 36), "46.4515".toDecimal()), // 19 Jan
            Value(from + (interval * 37), "45.7835".toDecimal()), // 20 Jan
            Value(from + (interval * 38), "45.3548".toDecimal()), // 21 Jan
            Value(from + (interval * 39), "44.0288".toDecimal()), // 22 Jan
            None(from + (interval * 40)), // 23 Jan
            None(from + (interval * 41)), // 24 Jan

            Value(from + (interval * 42), "44.1783".toDecimal()), // 25 Jan
            Value(from + (interval * 43), "44.2181".toDecimal()), // 26 Jan
            Value(from + (interval * 44), "44.5672".toDecimal()), // 27 Jan
            Value(from + (interval * 45), "43.4205".toDecimal()), // 28 Jan
            Value(from + (interval * 46), "42.6628".toDecimal()), // 29 Jan
            None(from + (interval * 47)), // 30 Jan
            None(from + (interval * 48)), // 31 Jan
            Value(from + (interval * 49), "43.1314".toDecimal()), // 01 Feb
        )

        should("13 Dec RSI equals None") {
            indicator.getValues(interval, from - interval, from - interval)
                .shouldEndWith(None(from - interval))
        }

        should("14 Dec RSI equals None") {
            indicator.getValues(interval, from, from)
                .shouldEndWith(None(from))
        }

        should("4 Jan RSI equals None") {
            val from = from + (interval * 21)
            indicator.getValues(interval, from, from)
                .shouldEndWith(None(from))
        }

        should("5 Jan RSI equals 70.533") {
            val from = from + (interval * 22)
            indicator.doTestLastValue("70.533".toDecimal(), interval, from, from)
        }

        should("6 Jan RSI equals 66.319") {
            val from = from + (interval * 22)
            indicator.doTestLastValue("66.319".toDecimal(), interval, from, from + interval)
        }

        should("7 Jan RSI equals 66.550") {
            val from = from + (interval * 22)
            indicator.doTestLastValue("66.550".toDecimal(), interval, from, from + interval * 2)
        }

        should("8 Jan RSI equals 69.406") {
            val from = from + (interval * 22)
            indicator.doTestLastValue("69.406".toDecimal(), interval, from, from + interval * 3)
        }

        should("9 Jan RSI equals None") {
            val from = from + (interval * 22)
            indicator.getValues(interval, from, from + interval * 4)
                .shouldEndWith(None(from + interval * 4))
        }

        should("10 Jan RSI equals None") {
            val from = from + (interval * 22)
            indicator.getValues(interval, from, from + interval * 5)
                .shouldEndWith(None(from + interval * 5))
        }

        should("11 Jan RSI equals 66.355") {
            val from = from + (interval * 22)
            indicator.doTestLastValue("66.355".toDecimal(), interval, from, from + interval * 6)
        }

        should("12 Jan RSI equals 57.975") {
            val from = from + (interval * 22)
            indicator.doTestLastValue("57.975".toDecimal(), interval, from, from + interval * 7)
        }

        should("13 Jan RSI equals 62.930") {
            val from = from + (interval * 22)
            indicator.doTestLastValue("62.930".toDecimal(), interval, from, from + interval * 8)
        }

        should("14 Jan RSI equals 63.257") {
            val from = from + (interval * 22)
            indicator.doTestLastValue("63.257".toDecimal(), interval, from, from + interval * 9)
        }

        should("15 Jan RSI equals 56.059") {
            val from = from + (interval * 22)
            indicator.doTestLastValue("56.059".toDecimal(), interval, from, from + interval * 10)
        }
    }
})
