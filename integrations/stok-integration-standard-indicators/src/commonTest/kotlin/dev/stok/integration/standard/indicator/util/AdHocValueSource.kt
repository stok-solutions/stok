package dev.stok.integration.standard.indicator.util

import dev.stok.indicator.IndicatorValue
import dev.stok.indicator.IndicatorValueSource
import dev.stok.typing.CandleInterval
import kotlinx.datetime.Instant

class AdHocValueSource<V : Any>(
    vararg var values: IndicatorValue<V>
) : IndicatorValueSource<V> {

    override suspend fun getValues(interval: CandleInterval, from: Instant, to: Instant): List<IndicatorValue<V>> =
        buildList list@{
            var moment = from
            while (moment <= to) {
                this@list += values.firstOrNull { it.moment == moment } ?: IndicatorValue.None(moment)
                moment += interval
            }
        }
}
