package dev.stok.integration.standard.indicator.impl

import dev.stok.CandleIntervals
import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.common.typing.DecimalValue
import dev.stok.indicator.IndicatorValue.None
import dev.stok.indicator.IndicatorValue.Value
import dev.stok.integration.standard.indicator.util.AdHocValueSource
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldContainAll
import io.kotest.matchers.collections.shouldHaveSize
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant

class ShiftedValuesSourceTests : ShouldSpec({

    val from = LocalDateTime.parse("2007-12-30T00:00:00").toInstant(TimeZone.UTC)
    val interval = CandleIntervals.INTERVAL_5MIN

    val originalSource = AdHocValueSource<DecimalValue>()

    should("zero shift does not change source") {
        originalSource.values = arrayOf(
            Value(from + (interval * 0), "0".toDecimal()),
            Value(from + (interval * 1), "1".toDecimal()),
            Value(from + (interval * 2), "2".toDecimal()),
            Value(from + (interval * 3), "3".toDecimal()),
        )

        ShiftedValueSource(originalSource, valuesShift = 0)
            .getValues(interval, from = from, to = from + (interval * 3))
            .shouldHaveSize(4)
            .shouldContainAll(originalSource.values.toList())
    }

    should("return previous value for value and shift(1)") {
        originalSource.values = arrayOf(
            Value(from + (interval * 0), "0".toDecimal()),
            Value(from + (interval * 1), "1".toDecimal()),
            Value(from + (interval * 2), "2".toDecimal()),
            Value(from + (interval * 3), "3".toDecimal()),
        )

        ShiftedValueSource(originalSource, valuesShift = 1)
            .getValues(interval, from = from, to = from + (interval * 3))
            .shouldHaveSize(4)
            .shouldContain(None(from)) // 0 -> None
            .shouldContain(Value(from + (interval * 1), "0".toDecimal())) // 1 -> 0
            .shouldContain(Value(from + (interval * 2), "1".toDecimal())) // 2 -> 1
            .shouldContain(Value(from + (interval * 3), "2".toDecimal())) // 3 -> 2
    }

    should("works good for shift(2)") {
        originalSource.values = arrayOf(
            Value(from + (interval * 0), "0".toDecimal()),
            Value(from + (interval * 1), "1".toDecimal()),
            Value(from + (interval * 2), "2".toDecimal()),
            Value(from + (interval * 3), "3".toDecimal()),
        )

        ShiftedValueSource(originalSource, valuesShift = 2)
            .getValues(interval, from = from, to = from + (interval * 3))
            .shouldHaveSize(4)
            .shouldContain(None(from)) // 0 -> None
            .shouldContain(None(from + (interval * 1))) // 1 -> None
            .shouldContain(Value(from + (interval * 2), "0".toDecimal())) // 2 -> 0
            .shouldContain(Value(from + (interval * 3), "1".toDecimal())) // 3 -> 1
    }

    should("values shifted on None element") {
        originalSource.values = arrayOf(
            Value(from + (interval * 0), "0".toDecimal()),
            Value(from + (interval * 1), "1".toDecimal()),
            None(from + (interval * 2)), // none appeared
            Value(from + (interval * 3), "3".toDecimal()),
        )

        ShiftedValueSource(originalSource, valuesShift = 1)
            .getValues(interval, from = from, to = from + (interval * 3))
            .shouldHaveSize(4)
            .shouldContain(None(from)) // 0 -> None
            .shouldContain(Value(from + (interval * 1), "0".toDecimal())) // 1 -> 0
            .shouldContain(Value(from + (interval * 2), "1".toDecimal())) // None -> 1
            .shouldContain(Value(from + (interval * 3), "1".toDecimal())) // 3 -> 1
    }
})
