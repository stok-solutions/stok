package dev.stok.integration.standard.indicator.impl

import dev.stok.common.kotlin.extension.div
import dev.stok.common.kotlin.extension.minus
import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.common.typing.DecimalValue
import dev.stok.indicator.Indicator
import dev.stok.indicator.IndicatorValue
import dev.stok.indicator.IndicatorValueSource
import dev.stok.indicator.filterValues
import dev.stok.indicator.util.computeValuesCount
import dev.stok.typing.CandleInterval
import kotlinx.datetime.Instant
import kotlin.collections.asSequence
import kotlin.sequences.lastOrNull

class EmaIndicator(
    private val valueSource: IndicatorValueSource<DecimalValue>,
    override val period: Int
) : Indicator<DecimalValue> {

    private val smaIndicator: Indicator<DecimalValue> = SmaIndicator(valueSource, period)

    override suspend fun getValues(
        interval: CandleInterval,
        from: Instant,
        to: Instant
    ): List<IndicatorValue<DecimalValue>> {
        val values: List<IndicatorValue<DecimalValue>> = valueSource.getValues(interval, from = from, to = to)
        val smaValues: List<IndicatorValue<DecimalValue>> = smaIndicator.getValues(interval, from = from, to = to)

        val capacity = computeValuesCount(interval, from, to)
        val result: MutableList<IndicatorValue<DecimalValue>> = ArrayList(capacity)

        val smoothingConst = smoothingConst(period)

        val valueAndSmaSeq = values.asSequence() zip smaValues.asSequence()
        for ((value, sma) in valueAndSmaSeq) {
            val previousEma = result.asSequence().filterValues().lastOrNull()
            result += when {
                (previousEma == null) -> sma
                value is IndicatorValue.Value<DecimalValue> -> {
                    val prevEmaVal = previousEma.value
                    val currentPrice = value.value
                    val currentEma = smoothingConst * currentPrice + prevEmaVal * (1 - smoothingConst)
                    IndicatorValue.Value(value.moment, currentEma)
                }
                else -> value // trick not to allocate new value
            }
        }

        return result
    }

    companion object {

        @Suppress("NOTHING_TO_INLINE")
        private inline fun smoothingConst(period: Int): DecimalValue =
            "2.0".toDecimal().div(period + 1)
    }
}
