package dev.stok.integration.standard.indicator.impl

import dev.stok.common.kotlin.extension.div
import dev.stok.common.kotlin.extension.sumOf
import dev.stok.common.typing.DecimalValue
import dev.stok.indicator.Indicator
import dev.stok.indicator.IndicatorValue
import dev.stok.indicator.IndicatorValueSource
import dev.stok.indicator.util.computeValuesCount
import dev.stok.integration.standard.indicator.kotlin.extension.computeAtMostValuesCount
import dev.stok.typing.CandleInterval
import kotlinx.datetime.Instant

class SmaIndicator(
    private val valueSource: IndicatorValueSource<DecimalValue>,
    override val period: Int
) : Indicator<DecimalValue> {

    override suspend fun getValues(
        interval: CandleInterval,
        from: Instant,
        to: Instant
    ): List<IndicatorValue<DecimalValue>> {
        val pastValues: List<IndicatorValue.Value<DecimalValue>> = valueSource.computeAtMostValuesCount(
            count = period - 1,
            toMoment = from - interval,
            interval = interval,
        )
        val values: List<IndicatorValue<DecimalValue>> = valueSource.getValues(interval, from = from, to = to)

        val capacity = computeValuesCount(interval, from, to)
        val result: MutableList<IndicatorValue<DecimalValue>> = ArrayList(capacity)

        val previousValues: MutableList<IndicatorValue.Value<DecimalValue>> = pastValues.toMutableList()

        for (currentIndex in values.indices) {
            val currentValue = values[currentIndex]
            if (currentValue is IndicatorValue.None) {
                result += currentValue // trick not to allocate new value
                continue
            }
            check(currentValue is IndicatorValue.Value)

            if (previousValues.size < (period - 1)) {
                previousValues += currentValue
                result += IndicatorValue.None(currentValue.moment)
                continue
            }

            val valuesToCompute = previousValues.takeLast(period - 1) + currentValue
            val smaValue = valuesToCompute.asSequence()
                .sumOf(IndicatorValue.Value<DecimalValue>::value)
                .div(valuesToCompute.size)

            previousValues += currentValue
            result += IndicatorValue.Value(currentValue.moment, smaValue)
        }

        return result
    }
}
