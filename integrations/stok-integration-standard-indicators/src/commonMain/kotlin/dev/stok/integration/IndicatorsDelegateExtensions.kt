package dev.stok.integration

import dev.stok.CandleIntervals
import dev.stok.MutableStokBootstrap
import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.common.typing.DecimalValue
import dev.stok.common.util.uncheckedCast
import dev.stok.context.BootstrapAwareContext
import dev.stok.dsl.context.StrategyContext
import dev.stok.dsl.model.IndicatorsDelegate
import dev.stok.indicator.ComputableArray
import dev.stok.indicator.InstrumentCandleSource
import dev.stok.indicator.impl.IndicatorArray
import dev.stok.indicator.impl.ValueSourceArray
import dev.stok.indicator.kotlin.extension.map
import dev.stok.integration.standard.indicator.JvmParameters
import dev.stok.integration.standard.indicator.QuoteApiInstrumentCachedCandleSource
import dev.stok.integration.standard.indicator.QuoteApiInstrumentCandleSource
import dev.stok.integration.standard.indicator.enum.CandlePriceType
import dev.stok.integration.standard.indicator.impl.EmaIndicator
import dev.stok.integration.standard.indicator.impl.MacdIndicator
import dev.stok.integration.standard.indicator.impl.PercentChangeValueSource
import dev.stok.integration.standard.indicator.impl.PriceValueSource
import dev.stok.integration.standard.indicator.impl.RsiIndicator
import dev.stok.integration.standard.indicator.impl.ShiftedValueSource
import dev.stok.integration.standard.indicator.impl.SmaIndicator
import dev.stok.integration.standard.indicator.impl.VolumeValueSource
import dev.stok.integration.standard.indicator.model.MacdIndicatorValue
import dev.stok.typing.Candle
import dev.stok.typing.CandleInterval
import dev.stok.typing.InstrumentId
import dev.stok.typing.Quantity
import kotlin.time.Duration
import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours

/**
 * [Investopedia. RSI](https://www.investopedia.com/terms/r/rsi.asp#toc-what-is-the-relative-strength-index-rsi)
 *
 * @param instrumentId
 * @param interval
 * @param fastEmaPeriod
 * @param slowEmaPeriod
 * @param signalEmaPeriod
 * @return
 *
 * @see
 */
context(StrategyContext)
fun IndicatorsDelegate.rsi(
    instrumentId: InstrumentId,
    interval: CandleInterval,
    period: Int = 14,
    priceType: CandlePriceType = CandlePriceType.CLOSE
): ComputableArray<DecimalValue> = IndicatorArray(
    indicator = RsiIndicator(
        valueSource = PriceValueSource(
            candleSource = getCandleSourceFor(instrumentId),
            priceType = priceType,
        ),
        period = period,
    ),
    interval = interval,
)

/**
 * [StockCharts. MACD (Moving Average Convergence/Divergence Oscillator)](https://school.stockcharts.com/doku.php?id=technical_indicators:moving_average_convergence_divergence_macd)
 *
 * @param instrumentId
 * @param interval
 * @param fastEmaPeriod
 * @param slowEmaPeriod
 * @param signalEmaPeriod
 * @return
 *
 * @see
 */
context(StrategyContext)
fun IndicatorsDelegate.macd(
    instrumentId: InstrumentId,
    interval: CandleInterval,
    fastEmaPeriod: Int = 12,
    slowEmaPeriod: Int = 26,
    signalEmaPeriod: Int = 9,
    priceType: CandlePriceType = CandlePriceType.CLOSE
): ComputableArray<MacdIndicatorValue> = IndicatorArray(
    indicator = MacdIndicator(
        valueSource = PriceValueSource(
            candleSource = getCandleSourceFor(instrumentId),
            priceType = priceType,
        ),
        fastEmaPeriod = fastEmaPeriod,
        slowEmaPeriod = slowEmaPeriod,
        signalEmaPeriod = signalEmaPeriod,
    ),
    interval = interval,
)

/**
 *
 * @param instrumentId
 * @param interval
 * @param period
 * @return
 */
context(StrategyContext)
fun IndicatorsDelegate.ema(
    instrumentId: InstrumentId,
    interval: CandleInterval,
    period: Int,
    priceType: CandlePriceType = CandlePriceType.CLOSE
): ComputableArray<DecimalValue> = IndicatorArray(
    indicator = EmaIndicator(
        valueSource = PriceValueSource(
            candleSource = getCandleSourceFor(instrumentId),
            priceType = priceType,
        ),
        period = period,
    ),
    interval = interval,
)

/**
 * [StockCharts. Moving Averages)](https://school.stockcharts.com/doku.php?id=technical_indicators:moving_averages)
 *
 * @param instrumentId
 * @param interval
 * @param period
 * @return
 */
context(StrategyContext)
fun IndicatorsDelegate.sma(
    instrumentId: InstrumentId,
    interval: CandleInterval,
    period: Int,
    priceType: CandlePriceType = CandlePriceType.CLOSE
): ComputableArray<DecimalValue> = IndicatorArray(
    indicator = SmaIndicator(
        valueSource = PriceValueSource(
            candleSource = getCandleSourceFor(instrumentId),
            priceType = priceType,
        ),
        period = period,
    ),
    interval = interval,
)

context(StrategyContext)
fun IndicatorsDelegate.volumePercentChange(
    instrumentId: InstrumentId,
    interval: CandleInterval,
    dateRange: Duration = getDefaultDateRange(interval)
): ComputableArray<DecimalValue> = ValueSourceArray(
    valueSource = run {
        val volumeSource = VolumeValueSource(getCandleSourceFor(instrumentId)).map(Quantity::toDecimal)
        PercentChangeValueSource(
            valueSource = volumeSource,
            previousValueSource = ShiftedValueSource(valueSource = volumeSource, valuesShift = 1),
        )
    },
    interval = interval,
    dateRange = dateRange,
)

context(StrategyContext)
fun IndicatorsDelegate.volume(
    instrumentId: InstrumentId,
    interval: CandleInterval,
    dateRange: Duration = getDefaultDateRange(interval)
): ComputableArray<Quantity> = ValueSourceArray(
    valueSource = VolumeValueSource(candleSource = getCandleSourceFor(instrumentId)),
    interval = interval,
    dateRange = dateRange,
)

context(StrategyContext)
fun IndicatorsDelegate.intervalPricePercentChange(
    instrumentId: InstrumentId,
    interval: CandleInterval,
    dateRange: Duration = getDefaultDateRange(interval),
    lookBehindIntervals: Int = 1,
    priceType: CandlePriceType = CandlePriceType.CLOSE,
    previousPriceType: CandlePriceType = CandlePriceType.CLOSE
): ComputableArray<DecimalValue> = ValueSourceArray(
    valueSource = PercentChangeValueSource(
        valueSource = PriceValueSource(
            candleSource = getCandleSourceFor(instrumentId),
            priceType = priceType,
        ),
        previousValueSource = ShiftedValueSource(
            PriceValueSource(
                candleSource = getCandleSourceFor(instrumentId),
                priceType = previousPriceType,
            ),
            valuesShift = lookBehindIntervals,
        ),
    ),
    interval = interval,
    dateRange = dateRange,
)

context(StrategyContext)
fun IndicatorsDelegate.pricePercentChange(
    instrumentId: InstrumentId,
    interval: CandleInterval,
    dateRange: Duration = getDefaultDateRange(interval)
): ComputableArray<DecimalValue> =
    intervalPricePercentChange(
        instrumentId = instrumentId,
        interval = interval,
        dateRange = dateRange,
        lookBehindIntervals = 0,
        previousPriceType = CandlePriceType.OPEN,
        priceType = CandlePriceType.CLOSE,
    )

context(StrategyContext)
fun IndicatorsDelegate.price(
    instrumentId: InstrumentId,
    interval: CandleInterval,
    dateRange: Duration = getDefaultDateRange(interval),
    priceType: CandlePriceType = CandlePriceType.CLOSE
): ComputableArray<DecimalValue> = ValueSourceArray(
    valueSource = PriceValueSource(
        candleSource = getCandleSourceFor(instrumentId),
        priceType = priceType,
    ),
    interval = interval,
    dateRange = dateRange,
)

context(StrategyContext)
fun IndicatorsDelegate.candles(
    instrumentId: InstrumentId,
    interval: CandleInterval,
    dateRange: Duration = getDefaultDateRange(interval)
): ComputableArray<Candle> = ValueSourceArray(
    valueSource = getCandleSourceFor(instrumentId),
    interval = interval,
    dateRange = dateRange,
)

private fun getDefaultDateRange(interval: CandleInterval): Duration =
    when (interval) {
        CandleIntervals.INTERVAL_1MIN,
        CandleIntervals.INTERVAL_2MIN,
        CandleIntervals.INTERVAL_5MIN,
        CandleIntervals.INTERVAL_10MIN,
        CandleIntervals.INTERVAL_15MIN,
        CandleIntervals.INTERVAL_30MIN -> 12.hours

        CandleIntervals.INTERVAL_1HOUR,
        CandleIntervals.INTERVAL_2HOUR,
        CandleIntervals.INTERVAL_4HOUR -> 1.days

        CandleIntervals.INTERVAL_1DAY -> 7.days

        CandleIntervals.INTERVAL_1WEEK -> 30.days

        else -> if (interval < CandleIntervals.INTERVAL_1WEEK) {
            30.days
        } else {
            error("Interval [$interval] is not supported")
        }
    }

context(BootstrapAwareContext)
private fun IndicatorsDelegate.getCandleSourceFor(instrumentId: InstrumentId): InstrumentCandleSource {
    val bootstrap = uncheckedCast<MutableStokBootstrap>(bootstrap)
    val bootstrapName = "instrumentCandleSources"

    if (!bootstrap.hasByName(bootstrapName)) {
        bootstrap.setByName<List<InstrumentCandleSource>>(bootstrapName, emptyList())
    }

    val bootstrapSources: List<InstrumentCandleSource> = bootstrap.getByName(bootstrapName)

    val foundSource = bootstrapSources.firstOrNull { it.instrumentId == instrumentId }
    if (foundSource != null) {
        return foundSource
    }

    val newSource = if (JvmParameters.candlesCacheEnabled) {
        QuoteApiInstrumentCachedCandleSource(
            quoteApi = quoteApi,
            instrumentId = instrumentId
        )
    } else {
        QuoteApiInstrumentCandleSource(
            quoteApi = quoteApi,
            instrumentId = instrumentId
        )
    }
    bootstrap.setByName(bootstrapName, bootstrapSources + newSource)

    return newSource
}
