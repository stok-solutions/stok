package dev.stok.integration.standard.indicator

import dev.stok.CandleIntervals
import dev.stok.common.kotlin.extension.mapAsync
import dev.stok.indicator.IndicatorValue
import dev.stok.indicator.InstrumentCandleSource
import dev.stok.indicator.enum.RoundingMode
import dev.stok.indicator.util.computeCandleMoment
import dev.stok.indicator.util.computeValuesCount
import dev.stok.integration.api.QuoteApi
import dev.stok.integration.model.CandleRequest
import dev.stok.typing.CandleInterval
import dev.stok.typing.InstrumentId
import dev.stok.typing.TimeAwareCandle
import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging
import io.ktor.util.collections.ConcurrentMap
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.datetime.Instant
import kotlin.time.Duration
import kotlin.time.Duration.Companion.days

internal class QuoteApiInstrumentCachedCandleSource(
    private val quoteApi: QuoteApi,
    override val instrumentId: InstrumentId
) : InstrumentCandleSource {

    private val candlesCache: MutableMap<CandleRequest, List<TimeAwareCandle>> = ConcurrentMap()

    override suspend fun getValues(
        interval: CandleInterval,
        from: Instant,
        to: Instant
    ): List<IndicatorValue<TimeAwareCandle>> {
        require(from <= to) { "The 'from' time must be before the 'to' time." }

        val foundCandles: List<TimeAwareCandle> = run {
            val request = CandleRequest(instrumentId, interval, from, to)
            getCachedCandles(request)
        }

        val capacity = computeValuesCount(interval, from, to)
        val result: MutableList<IndicatorValue<TimeAwareCandle>> = ArrayList(capacity)

        var candleIndex = 0
        var currentMoment = from
        while (currentMoment <= to) {
            if (candleIndex >= foundCandles.size) {
                result += IndicatorValue.None(currentMoment)
                currentMoment += interval
                continue
            }

            val currentCandle = foundCandles[candleIndex]
            val currentCandleTime = computeCandleMoment(
                moment = getCandleTime(currentCandle),
                interval, mode = RoundingMode.CLEVER,
            )

            when {
                (currentMoment < currentCandleTime) -> {
                    result += IndicatorValue.None(currentMoment)
                    currentMoment += interval
                }

                (currentMoment == currentCandleTime) -> {
                    result += IndicatorValue.Value(currentMoment, currentCandle)
                    currentMoment += interval
                    ++candleIndex
                }

                else -> ++candleIndex
            }
        }

        return result
    }

    private suspend fun getCachedCandles(request: CandleRequest): List<TimeAwareCandle> {
        val window = PartialRequestsWindow(request.interval)
        for (cachedRequest in candlesCache.keys) {
            window.addRequest(cachedRequest)
        }

        val cachedRequests = window.getSortedRequestParts(request)
        val cachedCandles = cachedRequests.flatMap { partRequest ->
            candlesCache[partRequest] ?: run {
                LOGGER.warn { "Cache starvation for instrument [$instrumentId]" }
                getCandlesAndCache(partRequest)
            }
        }

        val requestsToPerform = if (cachedRequests.isNotEmpty()) {
            window.getSortedUncoveredRequests(request)
        } else {
            listOf(request)
        }

        if (requestsToPerform.isEmpty()) {
            return cachedCandles
        }

        val slicedRequestsToPerform = requestsToPerform.flatMap(::sliceRequest)

        // Last request is never cached
        val lastRequest = slicedRequestsToPerform.last()
        val asyncLastRequestCandles = coroutineScope { async { getCandles(lastRequest) } }

        val newCachedCandles = slicedRequestsToPerform.dropLast(1)
            .mapAsync { req -> getCandlesAndCache(req) }
            .flatten()

        val lastRequestCandles = asyncLastRequestCandles.await()

        return buildList(capacity = cachedCandles.size + newCachedCandles.size + lastRequestCandles.size) {
            addAll(cachedCandles)
            addAll(newCachedCandles)
            addAll(lastRequestCandles)
            sortBy(::getCandleTime)
        }
    }

    private suspend fun getCandlesAndCache(request: CandleRequest): List<TimeAwareCandle> {
        val receivedCandles = getCandles(request)
        if (receivedCandles.isEmpty()) {
            cacheCandles(request, emptyList())
            return emptyList()
        }

        val minCandleTime = receivedCandles.minOf(::getCandleTime)
        val maxCandleTime = receivedCandles.maxOf(::getCandleTime)

        val leftEmptyRequest = request.copy(to = minCandleTime - request.interval)
        if (leftEmptyRequest.from <= leftEmptyRequest.to) {
            cacheCandles(leftEmptyRequest, emptyList())
        }

        val effectiveRequest = request.copy(from = minCandleTime, to = maxCandleTime)
        cacheCandles(effectiveRequest, receivedCandles)

        val rightEmptyRequest = request.copy(from = maxCandleTime + request.interval)
        if (rightEmptyRequest.from <= rightEmptyRequest.to) {
            cacheCandles(rightEmptyRequest, emptyList())
        }

        return receivedCandles
    }

    private suspend fun getCandles(request: CandleRequest): List<TimeAwareCandle> {
        LOGGER.trace { "Requesting candles from API [${quoteApi::class.simpleName}] for request [$request]" }
        return quoteApi.getCandles(request)
    }

    private fun cacheCandles(request: CandleRequest, candles: List<TimeAwareCandle>) {
        candlesCache[request] = candles
        if (candles.isNotEmpty()) {
            LOGGER.debug { "[${candles.size}] candles were cached for request: $request" }
        } else {
            LOGGER.trace { "Empty candle list was cached for request: $request" }
        }
    }

    private fun sliceRequest(request: CandleRequest): Sequence<CandleRequest> = sequence {
        require(request.from <= request.to) { "The 'from' time must be before the 'to' time." }

        val maxRequestInterval = computeMaxRequestInterval(request.interval)
        var currentFrom = request.from
        do {
            val currentTo = minOf(currentFrom + maxRequestInterval, request.to)
            yield(request.copy(from = currentFrom, to = currentTo))
            currentFrom = currentTo + request.interval
        } while (currentFrom <= request.to)
    }

    private fun computeMaxRequestInterval(interval: CandleInterval): Duration =
        when (interval) {
            CandleIntervals.INTERVAL_1MIN,
            CandleIntervals.INTERVAL_2MIN,
            CandleIntervals.INTERVAL_3MIN,
            CandleIntervals.INTERVAL_5MIN,
            CandleIntervals.INTERVAL_10MIN,
            CandleIntervals.INTERVAL_15MIN,
            CandleIntervals.INTERVAL_30MIN -> 1.days

            CandleIntervals.INTERVAL_1HOUR -> 7.days

            CandleIntervals.INTERVAL_2HOUR,
            CandleIntervals.INTERVAL_4HOUR -> 30.days

            CandleIntervals.INTERVAL_1DAY -> 365.days
            CandleIntervals.INTERVAL_1WEEK -> 365.days * 2
            CandleIntervals.INTERVAL_1MONTH -> 365.days * 10

            else -> interval * 100
        }

    companion object {

        private val LOGGER: KLogger = KotlinLogging.logger {}

        private fun getCandleTime(candle: TimeAwareCandle): Instant = candle.openTime
    }

    private class PartialRequestsWindow(private val interval: CandleInterval) {

        private val sortedPartRequests: MutableSet<CandleRequest> = sortedSetOf()

        fun isRequestCovered(request: CandleRequest): Boolean {
            val requests = getSortedRequestParts(request)
            if (requests.isEmpty()) return false

            return (requests.first().from <= request.from)
                .and(requests.last().to >= request.to)
        }

        fun getSortedRequestParts(request: CandleRequest): List<CandleRequest> {
            if (sortedPartRequests.isEmpty()) return emptyList()

            return buildList {
                var startTime = request.from
                val endTime = request.to
                for (partRequest in sortedPartRequests) {
                    if ((startTime >= endTime) || (partRequest.from > endTime)) break
                    if (partRequest.interval != interval) continue
                    if (partRequest.to < startTime) continue

                    startTime = maxOf(partRequest.to, startTime)
                    add(partRequest)
                }
            }
        }

        fun getSortedUncoveredRequests(request: CandleRequest): List<CandleRequest> {
            val sortedParts = getSortedRequestParts(request)
            if (sortedParts.isEmpty()) { // fast path
                return listOf(request)
            }

            // TODO: rethink. probably not good capacity
            val result: MutableList<CandleRequest> = ArrayList(sortedParts.size + 1)
            val pivotRequest = sortedParts.first()

            if (request.from < sortedParts.first().from) {
                result += pivotRequest.copy(
                    from = request.from,
                    to = sortedParts.first().from - interval,
                )
            }

            for (currentIndex in 0..(sortedParts.size - 2)) {
                val currentRequest = sortedParts[currentIndex]
                val nextRequest = sortedParts[currentIndex + 1]
                if (currentRequest.to < nextRequest.from) {
                    result += pivotRequest.copy(
                        from = currentRequest.to + interval,
                        to = nextRequest.from - interval,
                    )
                }
            }

            if (request.to > sortedParts.last().to) {
                result += pivotRequest.copy(
                    from = sortedParts.last().to + interval,
                    to = request.to,
                )
            }

            return result
        }

        fun addRequest(request: CandleRequest) {
            if (request.interval != interval) return
            if (isRequestCovered(request)) return
            sortedPartRequests += request
        }
    }
}
