package dev.stok.integration.standard.indicator

import dev.stok.indicator.IndicatorValue
import dev.stok.indicator.InstrumentCandleSource
import dev.stok.indicator.enum.RoundingMode
import dev.stok.indicator.util.computeCandleMoment
import dev.stok.indicator.util.computeValuesCount
import dev.stok.integration.api.QuoteApi
import dev.stok.integration.model.CandleRequest
import dev.stok.typing.CandleInterval
import dev.stok.typing.InstrumentId
import dev.stok.typing.TimeAwareCandle
import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.datetime.Instant

internal class QuoteApiInstrumentCandleSource(
    private val quoteApi: QuoteApi,
    override val instrumentId: InstrumentId
) : InstrumentCandleSource {

    override suspend fun getValues(
        interval: CandleInterval,
        from: Instant,
        to: Instant
    ): List<IndicatorValue<TimeAwareCandle>> {
        require(from <= to) { "The 'from' time must be before the 'to' time." }

        val foundCandles: List<TimeAwareCandle> = run {
            val request = CandleRequest(instrumentId, interval, from, to)
            getCandles(request)
        }

        val capacity = computeValuesCount(interval, from, to)
        val result: MutableList<IndicatorValue<TimeAwareCandle>> = ArrayList(capacity)

        var candleIndex = 0
        var currentMoment = from
        while (currentMoment <= to) {
            if (candleIndex >= foundCandles.size) {
                result += IndicatorValue.None(currentMoment)
                currentMoment += interval
                continue
            }

            val currentCandle = foundCandles[candleIndex]
            val currentCandleTime = computeCandleMoment(
                moment = getCandleTime(currentCandle),
                interval, mode = RoundingMode.CLEVER,
            )

            when {
                (currentMoment < currentCandleTime) -> {
                    result += IndicatorValue.None(currentMoment)
                    currentMoment += interval
                }

                (currentMoment == currentCandleTime) -> {
                    result += IndicatorValue.Value(currentMoment, currentCandle)
                    currentMoment += interval
                    ++candleIndex
                }

                else -> ++candleIndex
            }
        }

        return result
    }

    private suspend fun getCandles(request: CandleRequest): List<TimeAwareCandle> {
        LOGGER.trace { "Requesting candles from API [${quoteApi::class.simpleName}] for request [$request]" }
        return quoteApi.getCandles(request)
    }

    companion object {

        private val LOGGER: KLogger = KotlinLogging.logger {}

        private fun getCandleTime(candle: TimeAwareCandle): Instant = candle.openTime
    }
}
