package dev.stok.integration.standard.indicator.impl

import dev.stok.common.typing.DecimalValue
import dev.stok.indicator.Indicator
import dev.stok.indicator.IndicatorValue
import dev.stok.indicator.IndicatorValueSource
import dev.stok.indicator.util.computeValuesCount
import dev.stok.integration.standard.indicator.model.MacdIndicatorValue
import dev.stok.integration.standard.indicator.util.takeAnyNone
import dev.stok.typing.CandleInterval
import kotlinx.datetime.Instant

class MacdIndicator(
    valueSource: IndicatorValueSource<DecimalValue>,
    fastEmaPeriod: Int,
    slowEmaPeriod: Int,
    signalEmaPeriod: Int
) : Indicator<MacdIndicatorValue> {

    override val period: Int

    private val macdLineIndicator: Indicator<DecimalValue> =
        MacdLineIndicator(
            valueSource = valueSource,
            fastEmaPeriod = fastEmaPeriod,
            slowEmaPeriod = slowEmaPeriod,
        )

    private val signalLineIndicator: Indicator<DecimalValue> =
        EmaIndicator(valueSource = macdLineIndicator, period = signalEmaPeriod)

    init {
        this.period = maxOf(macdLineIndicator.period, signalLineIndicator.period)
    }

    override suspend fun getValues(
        interval: CandleInterval,
        from: Instant,
        to: Instant
    ): List<IndicatorValue<MacdIndicatorValue>> {
        val macdLineValues: List<IndicatorValue<DecimalValue>> = macdLineIndicator.getValues(interval, from, to)
        val signalLineValues: List<IndicatorValue<DecimalValue>> = signalLineIndicator.getValues(interval, from, to)

        val capacity = computeValuesCount(interval, from, to)
        val result: MutableList<IndicatorValue<MacdIndicatorValue>> = ArrayList(capacity)

        val macLineWithSignalLineSeq = macdLineValues.asSequence() zip signalLineValues.asSequence()
        for ((macdLine, signalLine) in macLineWithSignalLineSeq) {
            result += if (macdLine.hasValue() && signalLine.hasValue()) {
                val macdIndicatorValue = MacdIndicatorValue(
                    macd = macdLine.value!!,
                    signal = signalLine.value!!,
                    histogram = macdLine.value!! - signalLine.value!!,
                )
                IndicatorValue.Value(macdLine.moment, macdIndicatorValue)
            } else {
                takeAnyNone(macdLine, signalLine) // trick not to allocate new value
            }
        }

        return result
    }
}
