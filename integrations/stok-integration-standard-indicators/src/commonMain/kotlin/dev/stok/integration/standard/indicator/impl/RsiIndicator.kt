package dev.stok.integration.standard.indicator.impl

import dev.stok.common.kotlin.extension.absolute
import dev.stok.common.kotlin.extension.div
import dev.stok.common.kotlin.extension.minus
import dev.stok.common.kotlin.extension.plus
import dev.stok.common.kotlin.extension.sumOf
import dev.stok.common.kotlin.extension.times
import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.common.typing.DecimalValue
import dev.stok.indicator.Indicator
import dev.stok.indicator.IndicatorValue
import dev.stok.indicator.IndicatorValueSource
import dev.stok.indicator.filterValues
import dev.stok.indicator.util.computeValuesCount
import dev.stok.integration.standard.indicator.kotlin.extension.computeAtMostValuesCount
import dev.stok.typing.CandleInterval
import kotlinx.datetime.Instant

class RsiIndicator(
    private val valueSource: IndicatorValueSource<DecimalValue>,
    period: Int
) : Indicator<DecimalValue> {

    override val period: Int = period + 1

    private val rsiPeriod = period

    override suspend fun getValues(
        interval: CandleInterval,
        from: Instant,
        to: Instant
    ): List<IndicatorValue<DecimalValue>> {
        val pastValues: List<IndicatorValue.Value<DecimalValue>> = valueSource.computeAtMostValuesCount(
            count = rsiPeriod,
            toMoment = from - interval,
            interval = interval,
        )
        val values: List<IndicatorValue<DecimalValue>> = valueSource.getValues(interval, from = from, to = to)

        val capacity = computeValuesCount(interval, from, to)
        val result: MutableList<IndicatorValue<DecimalValue>> = ArrayList(capacity)
        val averageGains: MutableList<DecimalValue> = ArrayList(capacity)
        val averageLosses: MutableList<DecimalValue> = ArrayList(capacity)

        val previousValues: MutableList<IndicatorValue.Value<DecimalValue>> = pastValues.toMutableList()

        for (currentIndex in values.indices) {
            val currentValue = values[currentIndex]
            if (currentValue is IndicatorValue.None) {
                result += currentValue // trick not to allocate new value
                continue
            }
            check(currentValue is IndicatorValue.Value)

            if (previousValues.size < rsiPeriod) {
                previousValues += currentValue
                result += IndicatorValue.None(currentValue.moment)
                continue
            }

            val (avgGain, avgLoss) = when {
                (result.asSequence().filterValues().count() == 0) -> {
                    val valuesToCompute = previousValues.takeLast(rsiPeriod) + currentValue
                    averageGain(valuesToCompute) to averageLoss(valuesToCompute)
                }
                else -> {
                    val prevValue = previousValues.last()

                    val currentGain = averageGain(listOf(prevValue, currentValue))
                    val currentLoss = averageLoss(listOf(prevValue, currentValue))

                    Pair(
                        (averageGains.last() * (rsiPeriod - 1) + currentGain) / rsiPeriod,
                        (averageLosses.last() * (rsiPeriod - 1) + currentLoss) / rsiPeriod,
                    )
                }
            }

            previousValues += currentValue
            averageGains += avgGain
            averageLosses += avgLoss

            val rsiValue = countRsiValue(avgLoss, avgGain)
            result += IndicatorValue.Value(currentValue.moment, rsiValue)
        }

        return result
    }

    private fun countRsiValue(avgLoss: DecimalValue, avgGain: DecimalValue) =
        when {
            (avgLoss == 0.toDecimal()) -> 100.toDecimal()
            (avgGain == 0.toDecimal()) -> 0.toDecimal()
            else -> {
                val rsValue = avgGain / avgLoss
                100 - (100 / (1 + rsValue))
            }
        }

    private fun averageGain(values: List<IndicatorValue.Value<DecimalValue>>): DecimalValue =
        values.asSequence()
            .windowed(2)
            .sumOf { (prev, current) ->
                val curVal = current.value
                val prevVal = prev.value
                if (curVal > prevVal) {
                    curVal - prevVal
                } else {
                    0.toDecimal()
                }
            }
            .div(values.size - 1)

    private fun averageLoss(values: List<IndicatorValue.Value<DecimalValue>>): DecimalValue =
        values.asSequence()
            .windowed(2)
            .sumOf { (prev, current) ->
                val curVal = current.value
                val prevVal = prev.value
                if (curVal < prevVal) {
                    (curVal - prevVal).absolute()
                } else {
                    0.toDecimal()
                }
            }
            .div(values.size - 1)
}
