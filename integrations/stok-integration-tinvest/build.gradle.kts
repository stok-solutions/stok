plugins {
    id(BuildPlugins.STOK_LIBRARY)
    alias(libs.plugins.kotlinx.atomicfu)
}

kotlin {
    sourceSets {
        commonMain {
            dependencies {
                implementation(projects.stokIntegrationModel)
            }
        }
        jvmMain {
            dependencies {
                implementation(libs.tinvest.javaSdkCore)
                implementation(libs.kotlinx.atomicfu)
            }
        }
    }
}
