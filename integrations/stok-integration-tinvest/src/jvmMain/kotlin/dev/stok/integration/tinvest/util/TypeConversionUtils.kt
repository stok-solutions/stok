package dev.stok.integration.tinvest.util

import com.google.protobuf.Timestamp
import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.common.typing.DecimalValue
import kotlinx.datetime.Instant
import ru.tinkoff.piapi.contract.v1.Quotation
import java.math.BigDecimal

internal fun Timestamp.toInstant(): Instant =
    Instant.fromEpochSeconds(seconds, nanos)

internal fun Quotation.toBigDecimal(): BigDecimal = when {
    (units == 0L) && (nano == 0) -> BigDecimal.ZERO
    else -> BigDecimal.valueOf(units).add(BigDecimal.valueOf(nano.toLong(), 9))
}

internal fun Quotation.toDecimal(): DecimalValue =
    toBigDecimal().toString().toDecimal()

internal fun DecimalValue.toTinvestQuotation(): Quotation =
    TODO()
