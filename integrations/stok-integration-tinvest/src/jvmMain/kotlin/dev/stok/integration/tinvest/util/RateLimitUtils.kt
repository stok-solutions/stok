package dev.stok.integration.tinvest.util

import dev.stok.common.kotlinx.coroutines.DiscreteRateLimiter
import dev.stok.common.kotlinx.coroutines.RateLimiter
import dev.stok.integration.tinvest.JvmParameters
import kotlinx.coroutines.CoroutineScope
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds
import kotlin.time.TimeMark

fun CoroutineScope.rpsServiceLimiter(): RateLimiter =
    DiscreteRateLimiter(
        limit = JvmParameters.serviceRpsLimit,
        period = 1.seconds,
        clock = Clock.System,
    )

/**
 * Computes a time mark that points to start of the next minute e.g. (15:45:12 -> 15:46:00)
 */
internal fun computeRateLimitRefillTime(clock: Clock): TimeMark {
    val currentTime = clock.now()
    if (currentTime.toEpochMilliseconds() % 60_000 == 0L) {
        return InstantTimeMark(currentTime, clock)
    }

    val nextMinuteStart = run {
        val millis = (currentTime + 1.minutes).toEpochMilliseconds()
        Instant.fromEpochMilliseconds(millis - (millis % 60000))
    }
    return InstantTimeMark(nextMinuteStart, clock)
}

/**
 * @see kotlinx.datetime.InstantTimeMark
 */
private class InstantTimeMark(
    private val instant: Instant,
    private val clock: Clock
) : TimeMark {

    override fun elapsedNow(): Duration = clock.now() - instant

    override fun plus(duration: Duration): TimeMark = InstantTimeMark(instant + duration, clock)

    override fun minus(duration: Duration): TimeMark = InstantTimeMark(instant - duration, clock)
}
