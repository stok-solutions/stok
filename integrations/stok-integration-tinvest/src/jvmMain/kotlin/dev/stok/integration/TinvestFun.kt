package dev.stok.integration

import dev.stok.integration.context.BootstrapBuilderContext
import dev.stok.integration.stereotype.BootstrapDslMarker
import dev.stok.integration.tinvest.api.TinvestAccountApi
import dev.stok.integration.tinvest.api.TinvestInstrumentApi
import dev.stok.integration.tinvest.api.TinvestOrderApi
import dev.stok.integration.tinvest.api.TinvestPortfolioApi
import dev.stok.integration.tinvest.api.TinvestQuoteApi

fun BootstrapBuilderContext.tinvest(closure: TinvestContextBuilder.() -> Unit) {
    val context: BootstrapBuilderContext = this@tinvest

    val builder = TinvestContextBuilder()
    closure(builder)

    val domainContext = builder.build()

    if (domainContext.enableMarketData) {
        val instrumentApi = TinvestInstrumentApi(token = domainContext.token)
        context.setByName(BootstrapStandardNames.instrumentApi, instrumentApi)

        val quoteApi = TinvestQuoteApi(token = domainContext.token)
        context.setByName(BootstrapStandardNames.quoteApi, quoteApi)
    }

    if (domainContext.enableReadonlyAccess || domainContext.enableTrading) {
        val accountApi = TinvestAccountApi(token = domainContext.token)
        context.setByName(BootstrapStandardNames.accountApi, accountApi)

        val operationApi = TinvestPortfolioApi(token = domainContext.token)
        context.setByName(BootstrapStandardNames.portfolioApi, operationApi)
    }

    if (domainContext.enableTrading) {
        val orderApi = TinvestOrderApi(token = domainContext.token)
        context.setByName(BootstrapStandardNames.orderApi, orderApi)
    }
}

@BootstrapDslMarker
class TinvestContextBuilder {

    private lateinit var token: String

    private var enableMarketData: Boolean = true

    private var enableReadonlyAccess: Boolean = false

    private var enableTrading: Boolean = false

    fun token(value: String) = apply { this.token = value }

    fun enableMarketData(value: Boolean) = apply { this.enableMarketData = value }

    fun enableReadonlyAccess(value: Boolean) = apply { this.enableReadonlyAccess = value }

    fun enableTrading(value: Boolean) = apply { this.enableTrading = value }

    fun build(): TinvestContext =
        TinvestContext(
            token = token,
            enableMarketData = enableMarketData,
            enableReadonlyAccess = enableReadonlyAccess,
            enableTrading = enableTrading,
        )
}

data class TinvestContext(
    val token: String,
    val enableMarketData: Boolean,
    val enableReadonlyAccess: Boolean,
    val enableTrading: Boolean
)
