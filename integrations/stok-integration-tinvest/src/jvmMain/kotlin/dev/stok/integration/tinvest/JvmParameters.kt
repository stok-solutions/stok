package dev.stok.integration.tinvest

internal object JvmParameters {

    /**
     * Maximal allowed requests per minute to [instrument service](https://tinkoff.github.io/investAPI/head-instruments/)
     *
     * See also [Tinkoff Invest Api. Limits policy of Tinkoff Invest Api](https://tinkoff.github.io/investAPI/limits/)
     */
    val instrumentsServiceRpmLimit: Int = System.getProperty("stok.tinvest.instrumentsServiceRpmLimit", "200").toInt()

    /**
     * Maximal allowed requests per minute to [market data service](https://tinkoff.github.io/investAPI/head-marketdata/)
     *
     * See also [Tinkoff Invest Api. Limits policy of Tinkoff Invest Api](https://tinkoff.github.io/investAPI/limits/)
     */
    val marketDataServiceRpmLimit: Int = System.getProperty("stok.tinvest.marketDataServiceRpmLimit", "300").toInt()

    /**
     * Maximal allowed requests per minute to [operations service](https://tinkoff.github.io/investAPI/head-operations/)
     *
     * See also [Tinkoff Invest Api. Limits policy of Tinkoff Invest Api](https://tinkoff.github.io/investAPI/limits/)
     */
    val operationsServiceRpmLimit: Int = System.getProperty("stok.tinvest.operationsServiceRpmLimit", "200").toInt()

    /**
     * Maximal allowed requests per minute to [order service](https://tinkoff.github.io/investAPI/head-orders/)
     *
     * See also [Tinkoff Invest Api. Limits policy of Tinkoff Invest Api](https://tinkoff.github.io/investAPI/limits/)
     */
    val ordersServiceRpmLimit: Int = System.getProperty("stok.tinvest.ordersServiceRpmLimit", "100").toInt()

    /**
     * Maximal allowed requests per minute to [user service](https://tinkoff.github.io/investAPI/head-users/)
     *
     * See also [Tinkoff Invest Api. Limits policy of Tinkoff Invest Api](https://tinkoff.github.io/investAPI/limits/)
     */
    val usersServiceRpmLimit: Int = System.getProperty("stok.tinvest.usersServiceRpmLimit", "100").toInt()

    /**
     * Maximal amount of requests per second per one service
     * to avoid network errors because of requests spike.
     */
    val serviceRpsLimit: Int = System.getProperty("stok.tinvest.serviceRpsLimit", "10").toInt()
}
