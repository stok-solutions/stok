package dev.stok.integration.backtesting

import dev.stok.model.StokPosition
import dev.stok.typing.MoneyValue
import kotlinx.datetime.Instant

class BacktestingMemory(
    startTime: Instant?
) {

    val clock: BacktestingClock = BacktestingClock()
        .apply { startTime?.let(this::setCurrentTime) }

    val allMoney: MutableList<MoneyValue> = ArrayList()

    val positions: MutableList<StokPosition> = ArrayList()
}
