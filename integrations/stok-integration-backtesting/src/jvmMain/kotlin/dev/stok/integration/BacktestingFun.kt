package dev.stok.integration

import dev.stok.integration.backtesting.BacktestingAccountApi
import dev.stok.integration.backtesting.BacktestingMemory
import dev.stok.integration.backtesting.BacktestingOrderApi
import dev.stok.integration.backtesting.BacktestingPortfolioApi
import dev.stok.integration.backtesting.BacktestingQuoteApi
import dev.stok.integration.backtesting.enum.FileType
import dev.stok.integration.backtesting.model.CandleSourceDefinition
import dev.stok.integration.context.BootstrapBuilderContext
import dev.stok.integration.stereotype.BootstrapDslMarker
import dev.stok.typing.InstrumentId
import kotlinx.datetime.Instant
import java.nio.file.Path

fun BootstrapBuilderContext.backtesting(closure: BacktestingContextBuilder.() -> Unit) {
    val context: BootstrapBuilderContext = this@backtesting

    val builder = BacktestingContextBuilder()
    closure(builder)

    val domainContext = builder.build()

    val memory = BacktestingMemory(startTime = domainContext.startTime)

    context.setByName(BootstrapStandardNames.clock, memory.clock)
    context.setByName(BootstrapStandardNames.accountApi, BacktestingAccountApi())
    context.setByName(BootstrapStandardNames.portfolioApi, BacktestingPortfolioApi(memory))
    context.setByName(BootstrapStandardNames.orderApi, BacktestingOrderApi(memory))
    context.setByName(BootstrapStandardNames.quoteApi, BacktestingQuoteApi(memory))
}

@BootstrapDslMarker
class BacktestingContextBuilder {

    private val candleSourceDefinitions: MutableList<CandleSourceDefinition> = mutableListOf()

    private var defaultInstrumentId: InstrumentId? = null

    private var startTime: Instant? = null

    fun candlesFrom(
        path: Path,
        instrumentId: InstrumentId? = null,
        type: FileType = FileType.CSV
    ) = apply {
        candleSourceDefinitions += CandleSourceDefinition(path, instrumentId, type)
    }

    fun defaultInstrumentId(value: InstrumentId) = apply { this.defaultInstrumentId = value }

    fun startTime(value: Instant) = apply { this.startTime = value }

    fun build(): BacktestingContext =
        BacktestingContext(
            candleSourceDefinitions = candleSourceDefinitions.map { it.copy(instumentId = defaultInstrumentId) },
            defaultInstrumentId = defaultInstrumentId,
            startTime = startTime,
        )
}

data class BacktestingContext(
    val candleSourceDefinitions: List<CandleSourceDefinition>,
    private var defaultInstrumentId: InstrumentId?,
    val startTime: Instant?
)
