package dev.stok.integration.backtesting

import dev.stok.integration.api.PortfolioApi
import dev.stok.model.StokAccount
import dev.stok.model.StokPosition
import dev.stok.typing.CurrencyCode
import dev.stok.typing.MoneyValue

class BacktestingPortfolioApi(private val memory: BacktestingMemory) : PortfolioApi {

    override suspend fun getAllMoney(account: StokAccount): List<MoneyValue> {
        checkAccount(account)
        return memory.allMoney
    }

    override suspend fun getMoney(currency: CurrencyCode, account: StokAccount): MoneyValue {
        checkAccount(account)
        return memory.allMoney.single { it.currency == currency }
    }

    override suspend fun getPositions(account: StokAccount): List<StokPosition> {
        checkAccount(account)
        return memory.positions
    }
}
