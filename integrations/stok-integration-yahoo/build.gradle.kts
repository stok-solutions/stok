plugins {
    id(BuildPlugins.STOK_LIBRARY)
}

kotlin {
    sourceSets {
        commonMain {
            dependencies {
                implementation(projects.stokIntegrationModel)
            }
        }
        jvmMain {
            dependencies {
                implementation("com.yahoofinance-api:YahooFinanceAPI:3.17.0")
            }
        }
    }
}
