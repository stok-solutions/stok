package dev.stok.integration.executor.localexecutor.api

import dev.stok.StokBootstrap
import dev.stok.StokStrategyManager
import dev.stok.common.kotlin.extension.partitionOn
import dev.stok.common.stereotype.DistributedField
import dev.stok.common.stereotype.DistributedLock
import dev.stok.common.stereotype.withLock
import dev.stok.common.util.LocalDistributedField
import dev.stok.common.util.MutexDistributedLock
import dev.stok.common.util.delayUntilCondition
import dev.stok.common.util.tickerChannel
import dev.stok.enum.StrategyState
import dev.stok.integration.BootstrapStandardNames
import dev.stok.integration.api.ExecutorApi
import dev.stok.integration.event.LifecycleEvent
import dev.stok.integration.executor.localexecutor.JvmParameters
import dev.stok.integration.executor.localexecutor.model.LocalStokExecutionState
import dev.stok.integration.internal.event.BatchEventBus
import dev.stok.integration.internal.event.EventBus
import dev.stok.integration.internal.event.EventSource
import dev.stok.integration.internal.model.StokExecutionState
import dev.stok.integration.internal.stereotype.ExecutionStateAware
import dev.stok.integration.stereotype.CoroutineScopeAware
import dev.stok.stereotype.StatefulElement
import dev.stok.stereotype.StokEvent
import io.github.oshai.kotlinlogging.KLogger
import io.github.oshai.kotlinlogging.KotlinLogging
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope
import kotlinx.datetime.Clock

class LocalExecutorApi : ExecutorApi, StatefulElement<LocalExecutorApi> {

    private lateinit var scope: CoroutineScope

    private lateinit var executionState: StokExecutionState

    private lateinit var eventBus: EventBus<StokEvent>

    private lateinit var manager: StokStrategyManager

    private val stateField: DistributedField<StrategyState> get() = executionState.stateField

    private val eventLatch: DistributedLock = MutexDistributedLock()

    override suspend fun schedule(manager: StokStrategyManager) {
        initializeStateBeforeSchedule(manager)
        delayUntilExpectedStateAndLock(StrategyState.CREATED)
        transitState(targetState = StrategyState.SCHEDULED, shouldUnlock = false) {
            initializeState(manager)
        }
    }

    override suspend fun start() = transitState(StrategyState.SCHEDULED, StrategyState.PARKED) {
        launchInterruptsEmission()
        eventBus.subscribeOnEvents(::handleEvents)
    }

    override suspend fun shutdown() {
        val actionRequired = stateField.getActualValue() !in setOf(StrategyState.FINISHING, StrategyState.FINISHED)
        if (!actionRequired) {
            LOGGER.warn("Illegal resume noticed")
            return
        }

        delayUntilExpectedStateAndLock(StrategyState.PARKED)
        transitState(targetState = StrategyState.FINISHING, shouldUnlock = false)

        scope.launch { doShutdown() }
    }

    private suspend fun doShutdown() =
        transitState(StrategyState.FINISHING, StrategyState.FINISHED) {
            eventLatch.lock()
            close()
        }

    override suspend fun pause() {
        val actionRequired = stateField.getActualValue() !in setOf(StrategyState.PAUSING, StrategyState.PAUSED)
        if (!actionRequired) {
            LOGGER.warn("Illegal pause noticed")
            return
        }

        delayUntilExpectedStateAndLock(StrategyState.PARKED)
        transitState(targetState = StrategyState.PAUSING, shouldUnlock = false)

        scope.launch { doPause() }
    }

    private suspend fun doPause() =
        transitState(StrategyState.PAUSING, StrategyState.PAUSED) { eventLatch.lock() }

    override suspend fun resume() {
        val actionRequired = stateField.getActualValue() in setOf(StrategyState.PAUSING, StrategyState.PAUSED)
        if (!actionRequired) {
            LOGGER.warn("Illegal resume noticed")
            return
        }

        delayUntilExpectedStateAndLock(StrategyState.PAUSED)
        transitState(targetState = StrategyState.PARKED)
    }

    private suspend fun handleEvents(events: List<StokEvent>) {
        if (events.isEmpty()) return // fast path

        // Wait until strategy is parked (waiting for events)
        delayUntilExpectedStateAndLock(StrategyState.PARKED)
        // ... and then transit to running state
        transitState(targetState = StrategyState.RUNNING, shouldUnlock = false)

        try {
            val eventsPartitions: List<List<StokEvent>> = if (JvmParameters.sliceEventsPartitionOnEachInterrupt) {
                events.partitionOn { partition -> partition.any { it is LifecycleEvent.InterruptEvent } }
            } else {
                val plainEvents = events.filter { it !is LifecycleEvent.InterruptEvent }
                val pivotalInterrupt = events.last { it is LifecycleEvent.InterruptEvent }
                listOf(plainEvents + pivotalInterrupt)
            }

            if (JvmParameters.parallelEventsPartitionHandling) {
                supervisorScope {
                    val dispatchTime = manager.bootstrap.clock.now()
                    for (eventsPartition in eventsPartitions) {
                        eventsPartition.forEach { event -> event.dispatchTime = dispatchTime }
                        launch(start = CoroutineStart.ATOMIC) {
                            manager.strategy.handleEvents(manager, eventsPartition)
                        }
                    }
                }
            } else {
                for (partition in eventsPartitions) {
                    val dispatchTime = manager.bootstrap.clock.now()
                    partition.forEach { event -> event.dispatchTime = dispatchTime }
                    supervisorScope {
                        manager.strategy.handleEvents(manager, partition)
                    }
                }
            }
        } finally {
            transitState(expectedState = StrategyState.RUNNING, targetState = StrategyState.PARKED)
        }
    }

    /**
     * Launch interrupt events emitter
     */
    private fun launchInterruptsEmission() {
        scope.launch {
            for (it in scope.tickerChannel()) {
                if (stateField.getActualValue() == StrategyState.FINISHING) break
                eventLatch.withLock {
                    eventBus.registerEvent(LifecycleEvent.InterruptEvent())
                    eventBus.emitEvents()
                }
            }
        }
    }

    private suspend fun transitState(
        expectedState: StrategyState? = null,
        targetState: StrategyState,
        shouldUnlock: Boolean = true,
        beforeTransitionFn: (suspend () -> Unit)? = null
    ) {
        try {
            if (expectedState != null) {
                val lockedState = stateField.localValue
                if (lockedState != expectedState) {
                    failTransition(lockedState, targetState)
                }
            }

            beforeTransitionFn?.let { fn -> fn() }
            stateField.setNewValue(targetState)
        } finally {
            if (shouldUnlock) {
                stateField.lock.unlock()
            }
        }
    }

    private suspend fun delayUntilExpectedStateAndLock(expectedState: StrategyState) {
        while (true) {
            delayUntilCondition { stateField.getActualValue() == expectedState }

            stateField.lock.lock()
            val actualState = runCatching { stateField.getActualValue() }
                .onFailure { ex ->
                    LOGGER.trace(ex) { "Failed to delay until state [$expectedState]" }
                    stateField.lock.unlock()
                }
                .getOrThrow()

            if (actualState != expectedState) {
                stateField.lock.unlock()
            } else {
                break
            }
        }
    }

    private fun initializeStateBeforeSchedule(manager: StokStrategyManager) {
        this.manager = manager
        this.executionState = createExecutionState()
            .also { state -> autowireExecutionState(manager, state) }
    }

    private fun initializeState(manager: StokStrategyManager) {
        val scope = createCoroutineScope(manager.bootstrap)

        this.scope = scope
            .also { scope -> autowireCoroutineScope(manager.bootstrap, scope = scope) }

        this.eventBus = createEventBus(manager.bootstrap, scope)
            .also { bus -> visitEventSources(manager.bootstrap, bus) }
    }

    override fun subscribeOnEvents(handler: suspend (List<StokEvent>) -> Unit) {
        this.eventBus.subscribeOnEvents(handler)
    }

    override fun close() {
        this.eventBus.close()
        this.scope.cancel()
    }

    override fun clone(): LocalExecutorApi =
        LocalExecutorApi()

    companion object {

        private val LOGGER: KLogger = KotlinLogging.logger {}

        private fun createCoroutineScope(bootstrap: StokBootstrap): CoroutineScope {
            val coroutineScopeFn: () -> CoroutineScope =
                bootstrap.getByName(BootstrapStandardNames.coroutineScopeSupplier)
            return coroutineScopeFn()
        }

        private fun createExecutionState(): StokExecutionState =
            LocalStokExecutionState().also { executionState ->
                executionState.stateField = LocalDistributedField(StrategyState.CREATED)
            }

        private fun autowireExecutionState(manager: StokStrategyManager, state: StokExecutionState) {
            if (manager is ExecutionStateAware) {
                manager.setExecutionState(state)
            }
        }

        private fun autowireCoroutineScope(bootstrap: StokBootstrap, scope: CoroutineScope) {
            for ((key, element) in bootstrap.toMap()) {
                if (element is CoroutineScopeAware) {
                    LOGGER.debug { "Autowiring coroutine scope into element [$key]" }
                    element.setCoroutineScope(scope)
                }
            }
        }

        private fun visitEventSources(bootstrap: StokBootstrap, eventBus: EventBus<StokEvent>) {
            val eventSources: List<EventSource<StokEvent>> = bootstrap.toMap().values.asSequence()
                .filterIsInstance<EventSource<StokEvent>>()
                .filter { it !is EventBus<*> }
                .toList()

            for (eventSource in eventSources) {
                eventBus.addEventSource(eventSource)
            }
        }

        private fun createEventBus(bootstrap: StokBootstrap, scope: CoroutineScope): EventBus<StokEvent> =
            BatchEventBus(
                scope = scope,
                beforeRegister = { event ->
                    val hasClock = bootstrap.hasByName(BootstrapStandardNames.clock)
                    val clock = if (hasClock) {
                        bootstrap.clock
                    } else {
                        Clock.System
                    }

                    val currentTime = clock.now()
                    event.creationTime = currentTime
                },
            )

        private fun failTransition(currentState: StrategyState, targetState: StrategyState): Nothing =
            error("Cannot transit from state [$currentState] to state [$targetState]")
    }
}
