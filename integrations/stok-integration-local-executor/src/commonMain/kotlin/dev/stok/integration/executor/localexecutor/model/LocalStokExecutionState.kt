package dev.stok.integration.executor.localexecutor.model

import dev.stok.common.util.LocalDistributedField
import dev.stok.enum.StrategyState
import dev.stok.integration.internal.model.StokExecutionState

class LocalStokExecutionState : StokExecutionState {

    override lateinit var stateField: LocalDistributedField<StrategyState>
}
