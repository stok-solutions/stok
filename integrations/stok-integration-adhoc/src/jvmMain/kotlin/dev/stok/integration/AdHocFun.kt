package dev.stok.integration

import dev.stok.integration.adhoc.AdHocAccountApi
import dev.stok.integration.adhoc.AdHocOrderApi
import dev.stok.integration.adhoc.AdHocPortfolioApi
import dev.stok.integration.adhoc.AdHocQuoteApi
import dev.stok.integration.context.BootstrapBuilderContext

fun BootstrapBuilderContext.adHoc() {
    setByName(BootstrapStandardNames.accountApi, AdHocAccountApi())
    setByName(BootstrapStandardNames.portfolioApi, AdHocPortfolioApi())
    setByName(BootstrapStandardNames.orderApi, AdHocOrderApi())
    setByName(BootstrapStandardNames.quoteApi, AdHocQuoteApi())
}
