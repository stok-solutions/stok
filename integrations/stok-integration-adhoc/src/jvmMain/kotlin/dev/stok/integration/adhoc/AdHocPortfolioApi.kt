package dev.stok.integration.adhoc

import dev.stok.common.kotlin.extension.toDecimal
import dev.stok.integration.api.PortfolioApi
import dev.stok.model.StokAccount
import dev.stok.model.StokPosition
import dev.stok.typing.CurrencyCode
import dev.stok.typing.MoneyValue

class AdHocPortfolioApi : PortfolioApi {

    override suspend fun getAllMoney(account: StokAccount): List<MoneyValue> {
        return emptyList()
    }

    override suspend fun getMoney(currency: CurrencyCode, account: StokAccount): MoneyValue {
        return MoneyValue(amount = 0.toDecimal(), currency)
    }

    override suspend fun getPositions(account: StokAccount): List<StokPosition> {
        return emptyList()
    }
}
