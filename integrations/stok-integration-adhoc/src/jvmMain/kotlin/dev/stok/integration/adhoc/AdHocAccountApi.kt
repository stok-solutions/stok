package dev.stok.integration.adhoc

import dev.stok.integration.api.AccountApi
import dev.stok.integration.enum.AccountAccessLevel
import dev.stok.integration.enum.AccountStatus
import dev.stok.integration.model.StokAccountDetails
import dev.stok.integration.model.StokAccountSummary

class AdHocAccountApi : AccountApi {

    override suspend fun getAccounts(): List<StokAccountSummary> =
        listOf(
            StokAccountSummary(id = "test", name = "test"),
        )

    override suspend fun getAccountById(id: String): StokAccountDetails =
        StokAccountDetails(id = "test", name = "test", AccountStatus.ACTIVE, AccountAccessLevel.FULL_ACCESS)
}
