# Stok

Stok is an asynchronous framework for creating trading strategies, stock market notifiers, backtesting and more.
Provides general Written in Kotlin from the ground up.

```kt
import dev.stok.*
import dev.stok.dsl.*
import dev.stok.typing.*
import dev.stok.indicator.*
import dev.stok.integration.*
import kotlin.time.Duration.Companion.seconds

fun main(args: Array<String>) {
    val manager = strategy {
        everyPeriod(30.seconds) {
            val priceIndicator = indicators.price(Ticker("APPL"), CandleIntervals.INTERVAL_1DAY)
            println("Current price of Apple stocks: ${priceIndicator.getCurrent().value}")
        }
    } manageOn bootstrap {
        localExecutor()
        tinvest {
            token("<YOUR API TOKEN>")
            enableMarketData(true)
        }
    }

    manager.start(wait = true)
}
```

## Using in your projects

### Gradle

Add dependencies (you can also add other modules that you need):

```kt
dependencies {
    implementation("dev.stok:stok-core:0.2.0-SNAPSHOT")
}
```

Make sure that you have mavenCentral() in the list of repositories:

```kt
repositories {
    mavenCentral()
}
```

## Principles

TBD
